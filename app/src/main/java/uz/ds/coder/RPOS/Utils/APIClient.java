package uz.ds.coder.RPOS.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import uz.ds.coder.RPOS.Product.Agent;
import uz.ds.coder.RPOS.Product.Client;
import uz.ds.coder.RPOS.Product.GroupRequest;
import uz.ds.coder.RPOS.Product.Income;
import uz.ds.coder.RPOS.Product.Product;
import uz.ds.coder.RPOS.Product.Sale;
import uz.ds.coder.RPOS.Product.SalesRequest;
import uz.ds.coder.RPOS.Product.Shift;
import uz.ds.coder.RPOS.models.SaleHistory;


public interface APIClient {

    @GET("api/fastproducts/")
    Call<List<Product>> getlistProducts();

    @GET("api/shifts/current/")
    Call<Shift> getCurrentShift();

    @POST("api/shifts/")
    Call<Shift> openShift();

    @GET("api/groups/")
    Call<GroupRequest> getGroups();

    @GET("api/clients/")
    Call<List<Client>> getlistClients();

    @GET("api/agents/")
    Call<List<Agent>> getlistAgents();

    @GET("api/sales/")
    Call<SalesRequest> getListSales(@Query("page") Integer page);

    @GET("api/sales/{id}/")
    Call<SaleHistory> getSale(@Path("id") int saleId);

    @PUT("api/sales/{id}/")
    Call<Sale> saveSale(@Path("id") int saleId, @Body Sale sale);

    @DELETE("api/sales/{id}/")
    Call<Sale> deleteSale(@Path("id") int saleId);

    @POST("api/agents/")
    Call<Agent> sendAgent(@Body Agent agent);

    @POST("api/clients/")
    Call<Client> sendClient(@Body Client client);

    @POST("api/sales/")
    Call<Sale> sendSale(@Body Sale sale);

    @POST("api/incomes/")
    Call<Income> sendIncome(@Body Income sale);

    @Headers("Content-Type: application/json")
    @POST("/api/shifts/open/")
    Call<Shift> openShift(@Body Shift shift);

    @POST("/shifts/close/")
    Call<Shift> closeShift(@Body Shift shift);
}






