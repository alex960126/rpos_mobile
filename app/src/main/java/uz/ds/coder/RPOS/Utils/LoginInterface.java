package uz.ds.coder.RPOS.Utils;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by bakha on 13.01.2018.
 */

public interface LoginInterface {
    @Headers("Content-Type: application/json")
    @POST("/api-auth/")
    Call<Token> getToken(@Body String body);
}
