package uz.ds.coder.RPOS.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import uz.ds.coder.RPOS.Activity.IncomeActivity;
import uz.ds.coder.RPOS.Product.Product;
import uz.ds.coder.RPOS.R;

/**
 * Created by Coder on 14-Mar-18.
 */

public class IncomeCartAdapter extends RecyclerView.Adapter<IncomeCartAdapter.ViewHolder> {
    private List<Product> products;
    private Context context;


    public IncomeCartAdapter(Context context, List<Product> products)  {
        this.products = products;
        this.context = context;

    }

    // Create new views (invoked by the layout manager)
    @Override
    public IncomeCartAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_item, parent, false);
        return new IncomeCartAdapter.ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(IncomeCartAdapter.ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Product product = products.get(position);
        holder.title.setText(product.getTitle());
        holder.price.setText(String.format("%d", product.getPrice().intValue()));
        holder.amount.setText(String.format("%d", product.getCount().intValue()));
        Double total = (product.getPrice() * product.getCount());
        holder.total.setText(String.format("%d", total.intValue()));
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((IncomeActivity)context).deleteProductFromCart(product);
            }
        });
        holder.price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog(product,0);
            }
        });
        holder.amount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialog(product, 1);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return products.size();
    }

    public void changeProduct(List<Product> products) {
        this.products = products;
    }

    // Provide a reference to the views for each data cart_item
    // Complex data items may need more than one view per cart_item, and
    // you provide access to all the views for a data cart_item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data cart_item is just a string in this case
        View mView;
        TextView title;
        TextView price;
        TextView amount;
        TextView total;
        ImageButton deleteButton;
        public ViewHolder(View v) {
            super(v);
            mView = v.findViewById(R.id.content_holder);
            title = (TextView) v.findViewById(R.id.title);
            amount = (TextView) v.findViewById(R.id.count);
            price = (TextView) v.findViewById(R.id.price);
            deleteButton = v.findViewById(R.id.delete);
            total = v.findViewById(R.id.total);
        }
    }

    String[] dialogTitles = new String[]{"цену", "количество"};

    private void openDialog(final Product product, final int type){
        LayoutInflater inflater = LayoutInflater.from(context);
        View subView = inflater.inflate(R.layout.change_price, null);
        final EditText subEditText = subView.findViewById(R.id.price);
        subEditText.setText((type == 0) ? product.getBuyprice().toString() : product.getCount().toString());
        subEditText.selectAll();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(String.format("Изменить %s", dialogTitles[type]));
        builder.setMessage(String.format("Введите %s", dialogTitles[type]));
        builder.setView(subView);
        AlertDialog alertDialog = builder.create();

        builder.setPositiveButton("Сохранить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (type == 0) {
                    product.setBuyprice(Double.valueOf(subEditText.getText().toString()));
                } else {
                    product.setCount(Double.valueOf(subEditText.getText().toString()));
                }
                ((IncomeActivity)context).refreshCart();
            }
        });

        builder.setNegativeButton("Отменить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(context, "Cancel", Toast.LENGTH_LONG).show();
            }
        });

        builder.show();
    }

}

