package uz.ds.coder.RPOS.Adapter;

import android.app.Fragment;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import uz.ds.coder.RPOS.Product.Client;
import uz.ds.coder.RPOS.Product.Product;
import uz.ds.coder.RPOS.R;
import uz.ds.coder.RPOS.Utils.TextFormatters;

/**
 * Created by Coder on 14-Mar-18.
 */

public class SaleProductsAdapter extends RecyclerView.Adapter<SaleProductsAdapter.ViewHolder> {
    public static List<Product> cart;
    private Context context;
    private Fragment fragment;
    private Client client;
    private boolean isEdit;


    public SaleProductsAdapter(Context context, List<Product> cart, Client client, boolean isEdit, Fragment fragment)  {
        this.cart = cart;
        this.context = context;
        this.fragment = fragment;
        this.client = client;
        this.isEdit = isEdit;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SaleProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.products_list_item, parent, false);
        return new SaleProductsAdapter.ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(SaleProductsAdapter.ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Product product = cart.get(position);
        holder.title.setText(product.getTitle());

        holder.count.setText(product.getCount() + "");
        Double price = product.getPrice();
        if (client.getType() == 1) {
            price = product.getPricesrd();
        } else if (client.getType() == 2) {
            price = product.getPriceopt();
        }
        holder.price.setText(TextFormatters.priceFormatter().format(price));
        holder.total.setText(TextFormatters.priceFormatter().format(price * product.getCount()));
        holder.deleteButton.setOnClickListener(v -> {
            cart.remove(position);
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return cart.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data cart_item is just a string in this case
        View mView;
        TextView title;
        TextView count;
        TextView price;
        TextView total;
        ImageButton deleteButton;
        public ViewHolder(View v) {
            super(v);
            mView = v.findViewById(R.id.content_holder);
            title = v.findViewById(R.id.pr_title);
            count = v.findViewById(R.id.pr_count);
            price = v.findViewById(R.id.pr_price);
            total = v.findViewById(R.id.pr_total);
            deleteButton = v.findViewById(R.id.pr_delete_btn);
        }
    }

    String[] dialogTitles = new String[]{"цену", "количество"};

    private void openDialog(final Product sale, final int type){
        LayoutInflater inflater = LayoutInflater.from(context);
        View subView = inflater.inflate(R.layout.change_price, null);
        final EditText subEditText = subView.findViewById(R.id.price);
        subEditText.setText((type == 0) ? sale.getPrice().toString() : sale.getCount().toString());
        subEditText.selectAll();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(String.format("Изменить %s", dialogTitles[type]));
        builder.setMessage(String.format("Введите %s", dialogTitles[type]));
        builder.setView(subView);
        builder.create();

        builder.setPositiveButton("Сохранить", (dialog, which) -> {
            if (type == 0) {
                sale.setPrice(Double.valueOf(subEditText.getText().toString()));
            } else {
                sale.setCount(Double.valueOf(subEditText.getText().toString()));
            }
            sale.setTotal(sale.getCount() * sale.getPrice() * (1 - sale.getDiscount() / 100));

        });

        builder.setNegativeButton("Отменить", (dialog, which) -> Toast.makeText(context, "Cancel", Toast.LENGTH_LONG).show());

        builder.show();
    }

}

