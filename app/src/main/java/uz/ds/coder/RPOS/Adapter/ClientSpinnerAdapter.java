package uz.ds.coder.RPOS.Adapter;

/**
 * Created by Bakhrom on 16.03.2018.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import uz.ds.coder.RPOS.Product.Client;

import static android.R.layout.simple_spinner_item;

public class ClientSpinnerAdapter extends ArrayAdapter<Client> {

    private LayoutInflater flater;

    public ClientSpinnerAdapter(Activity context,int resouceId, int textviewId, List<Client> list){

        super(context,resouceId,textviewId, list);
        flater = context.getLayoutInflater();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Client rowItem = getItem(position);

        @SuppressLint("ViewHolder") View rowview = flater.inflate(simple_spinner_item,null,true);

        TextView txtTitle = (TextView) rowview.findViewById(android.R.id.text1);
        assert rowItem != null;
        txtTitle.setText(rowItem.getFullname());

        return rowview;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = flater.inflate(android.R.layout.simple_spinner_dropdown_item,parent, false);
        }
        Client rowItem = getItem(position);
        TextView txtTitle = convertView.findViewById(android.R.id.text1);
        assert rowItem != null;
        txtTitle.setText(rowItem.getFullname());
        return convertView;
    }

}