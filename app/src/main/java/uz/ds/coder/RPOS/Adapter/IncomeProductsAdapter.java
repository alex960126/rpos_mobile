package uz.ds.coder.RPOS.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import uz.ds.coder.RPOS.Activity.IncomeActivity;
import uz.ds.coder.RPOS.Product.Product;
import uz.ds.coder.RPOS.R;

/**
 * Created by Coder on 06-Mar-18.
 */

public class IncomeProductsAdapter extends RecyclerView.Adapter<IncomeProductsAdapter.ViewHolder> {

    private List<Product> products;
    private Context context;
    public static List<Product> selectedProducts;
    final List<Product> templist=new ArrayList<>();

    public IncomeProductsAdapter(List<Product> products, Context context) {
        this.products = products;
        this.context = context;
        selectedProducts = new ArrayList<>();
        templist.addAll(products);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public IncomeProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_item, parent, false);
        return new ViewHolder(view);
    }

    public void init(){
        templist.addAll(products);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Product product = products.get(position);
        holder.title.setText(product.getTitle());
        holder.price.setText(String.format("%d", product.getPrice().intValue()));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            if (selectedProducts.contains(product)) {
                Product product1 = selectedProducts.get(selectedProducts.indexOf(product));
                selectedProducts.get(selectedProducts.indexOf(product)).setCount(product1.getCount() + 1);
            } else {
                product.setDiscount(((IncomeActivity)context).discount);
                product.setCount(1.0);
                selectedProducts.add(product);
            }
            ((IncomeActivity)context).addProductToCart();
            }
        });

    }

    public Filter getFilter() {

        Filter mfilter = new Filter() {
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                products.clear();
                products.addAll((ArrayList<Product>)results.values);
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                ArrayList<Product> res;

                res = new ArrayList<>();
                if(constraint.length()>0){
                    for(Product d: templist){

                        if(d.getTitle().toUpperCase().contains(constraint.toString().toUpperCase())
                                || d.getArticle().toUpperCase().contains(constraint.toString().toUpperCase())
                                || d.getBarcode().toUpperCase().contains(constraint.toString().toUpperCase())
                                || d.getGroup().getTitle().toUpperCase().contains(constraint.toString().toUpperCase())){
                            res.add(d);
                        }
                    }
                    result.count = res.size();
                    result.values = res;
                }

                if(constraint.length() == 0){
                    res.addAll(templist);
                    result.count = res.size();
                    result.values = res;
                }

                return result;
            }
        };

        return mfilter;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return products.size();
    }

    public void changeProduct(List<Product> products) {
        this.products = products;
    }

    // Provide a reference to the views for each data cart_item
    // Complex data items may need more than one view per cart_item, and
    // you provide access to all the views for a data cart_item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data cart_item is just a string in this case
        View mView;
        TextView title;
        TextView price;
        public ViewHolder(View v) {
            super(v);
            mView = v.findViewById(R.id.content_holder);
            title = (TextView) v.findViewById(R.id.title);
            price = (TextView) v.findViewById(R.id.price);
        }
    }
}
