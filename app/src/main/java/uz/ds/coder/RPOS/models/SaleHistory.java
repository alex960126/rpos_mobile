
package uz.ds.coder.RPOS.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.android.databinding.library.baseAdapters.BR;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import uz.ds.coder.RPOS.Product.Client;
import uz.ds.coder.RPOS.Product.Payment;
import uz.ds.coder.RPOS.Product.Product;
import uz.ds.coder.RPOS.Product.Shift;
import uz.ds.coder.RPOS.Utils.TextFormatters;


public class SaleHistory extends BaseObservable{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("cart")
    @Expose
    private List<Product> cart = null;
    @SerializedName("payment")
    @Expose
    private Payment payment;
    @SerializedName("client")
    @Expose
    private Client client;
    @SerializedName("agent")
    @Expose
    private Integer agent;

    public Integer getAgent() {
        return agent;
    }

    public void setAgent(Integer agent) {
        this.agent = agent;
    }

    @SerializedName("shift")
    @Expose
    private Shift shift;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("print")
    @Expose
    private Boolean print;

    @SerializedName("total")
    @Expose
    private Double total;
    @SerializedName("subtotal")
    @Expose
    private Double subtotal;
    @Bindable
    @SerializedName("done")
    @Expose
    private Boolean done;
    @SerializedName("discount")
    @Expose
    private Double discount;

    private Boolean isEdit;

    @Bindable
    public Boolean getEdit() {
        return isEdit;
    }

    public void setEdit(Boolean edit) {
        isEdit = edit;
        notifyPropertyChanged(BR.edit);
    }

    @Bindable
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }

    public List<Product> getCart() {
        return cart;
    }

    public void setCart(List<Product> cart) {
        this.cart = cart;
    }
    @Bindable
    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
        notifyPropertyChanged(BR.payment);
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
        notifyPropertyChanged(BR.client);
    }
    @Bindable
    public Shift getShift() {
        return shift;
    }

    public void setShift(Shift shift) {
        this.shift = shift;
        notifyPropertyChanged(BR.shift);
    }
    @Bindable
    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
        notifyPropertyChanged(BR.created);
    }
    @Bindable
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
        notifyPropertyChanged(BR.type);
    }
    @Bindable
    public Boolean getPrint() {
        return print;
    }

    public void setPrint(Boolean print) {
        this.print = print;
        notifyPropertyChanged(BR.print);
    }

    public Double getTotalDouble() {
        return total;
    }

    @Bindable
    public String getTotal() {
        return TextFormatters.priceFormatter().format(total);
    }

    public void setTotal(Double total) {
        this.total = total;
        notifyPropertyChanged(BR.total);
    }

    @Bindable
    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
        notifyPropertyChanged(BR.subtotal);
    }

    @Bindable
    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
        notifyPropertyChanged(BR.discount);
    }

    @Bindable
    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
        notifyPropertyChanged(BR.done);
    }

}
