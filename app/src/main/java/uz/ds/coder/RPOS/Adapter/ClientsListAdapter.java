package uz.ds.coder.RPOS.Adapter;
/**
 * Created by Bakhrom on 16.03.2018.
 */

import android.support.v7.app.AlertDialog;
import android.content.Context;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import uz.ds.coder.RPOS.Fragment.PosFragment;
import uz.ds.coder.RPOS.Product.Client;
import uz.ds.coder.RPOS.R;

public class ClientsListAdapter extends BaseAdapter implements Filterable {

    private List<Client> list = new ArrayList<>();
    private List<Client> originalValues;
    private LayoutInflater inflater;
    private AlertDialog dialog;
    private Fragment fragment;

    public ClientsListAdapter(Context context, Fragment fragment, AlertDialog dialog, List<Client> list){
        this.list = list;
        Context context1 = context;
        this.fragment = fragment;
        inflater = LayoutInflater.from(context);
        this.dialog = dialog;
        originalValues = new ArrayList<>();

        init();

    }

    @Override
    public int getCount() {
        Log.e("TAG", list.size() + "");
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    private class ViewHolder {
        TextView textView;
    }

    private void init(){
        originalValues.addAll(list);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null) {

            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.client_list_item, null);

            holder.textView = convertView
                    .findViewById(R.id.client_fullname);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        convertView.setOnClickListener(view -> {
            ((PosFragment)fragment).setClient(list.get(position));
            dialog.dismiss();
        });
        holder.textView.setText(list.get(position).getFullname());
        return convertView;
    }




    public Filter getFilter() {

        Filter mfilter = new Filter() {
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                list.clear();
                list.addAll((ArrayList<Client>)results.values);
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                ArrayList<Client> res;

                res = new ArrayList<>();
                if(constraint.length()>0){
                    for(Client d: originalValues){
                        Log.e("TAG1", d.getFullname());
                        boolean isFullname = (d.getFullname() != null) && d.getFullname().toUpperCase().contains(constraint.toString().toUpperCase());
                        if(isFullname) {
                            res.add(d);
                        }
                    }
                    result.count = res.size();
                    result.values = res;
                }
                Log.e("TAG", constraint.toString());
                if(constraint.length() == 0){
                    res.addAll(originalValues);
                    result.count = res.size();
                    result.values = res;
                }

                return result;
            }
        };

        return mfilter;
    }

}