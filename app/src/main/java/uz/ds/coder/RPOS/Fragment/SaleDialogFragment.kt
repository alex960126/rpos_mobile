package uz.ds.coder.RPOS.Fragment


import android.app.DialogFragment
import android.content.DialogInterface
import android.content.SharedPreferences
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_sale_dialog.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import uz.ds.coder.RPOS.Adapter.SaleProductsAdapter
import uz.ds.coder.RPOS.Product.Sale
import uz.ds.coder.RPOS.R
import uz.ds.coder.RPOS.Utils.APIClient
import uz.ds.coder.RPOS.Utils.KeyboardHelper
import uz.ds.coder.RPOS.Utils.ServiceGenerator
import uz.ds.coder.RPOS.databinding.FragmentSaleDialogBinding
import uz.ds.coder.RPOS.models.SaleHistory

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class SaleDialogFragment : DialogFragment() {

    private var saleId: Int = -1
    private lateinit var pref: SharedPreferences
    private lateinit var token: String
    private var responseCode = -1
    private lateinit var rv: RecyclerView
    private lateinit var sale: SaleHistory
    private lateinit var v: View
    private lateinit var binding : FragmentSaleDialogBinding

    companion object{

        fun newInstance(saleId: Int): SaleDialogFragment {
            val f = SaleDialogFragment()

            // Supply num input as an argument.
            val args = Bundle()
            args.putInt("sale_id", saleId)
            f.setArguments(args)

            return f
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding  = DataBindingUtil.inflate(inflater, R.layout.fragment_sale_dialog, container, false)
        var view = binding.root
        v = view
        pref = PreferenceManager.getDefaultSharedPreferences(activity)
        token = pref.getString("TOKEN","")
        saleId = arguments.getInt("sale_id", -1)

        rv = view.products_list
        rv.layoutManager = LinearLayoutManager(activity)
        v.edit_btn.setOnClickListener {
            sale.edit = !sale.edit
        }
        v.toggle_done_btn.setOnClickListener {
            toggleDone()
        }
        v.cancel_btn.setOnClickListener {
            dismiss()
        }
        v.save_btn.setOnClickListener {
            sale.edit = false
            //dismiss()
        }
        v.pay_loan_btn.setOnClickListener {
            if (v.payloan_layout.visibility == View.GONE) {
                v.payloan_layout.visibility = View.VISIBLE
                v.loan_amount_et.requestFocus()
                v.loan_amount_et.selectAll()
                KeyboardHelper.showKeyboard(activity, v.loan_amount_et)
            } else {
                if (v.loan_amount_et.hasFocus()) {
                    v.loan_amount_et.clearFocus()
                    KeyboardHelper.hideSoftKeyboard(activity, v.loan_amount_et)
                }
                v.payloan_layout.visibility = View.GONE
                v.loan_amount_et.text.clear()
            }
        }
        v.delete_btn.setOnClickListener {
            deleteSale()
        }
        v.save_loan_btn.setOnClickListener {
            if (v.loan_amount_et.hasFocus()) {
                v.loan_amount_et.clearFocus()
            }
        }
        loadSale()

        return view
    }



    fun payLoan() {

    }

    fun deleteSale() {
        ServiceGenerator.createService(APIClient::class.java, token, activity).deleteSale(sale.id).enqueue(object : Callback<Sale> {

            override fun onResponse(call: Call<Sale>, response: Response<Sale>) {
                if (response.isSuccessful) {
                    Toast.makeText(activity, getString(R.string.sale_delete_success_text), Toast.LENGTH_SHORT).show()
                    dismiss()
                }
            }

            override fun onFailure(call: Call<Sale>, t: Throwable) {
                v.progress.visibility = View.GONE
                Toast.makeText(activity, "Ошибка ${t.message}", Toast.LENGTH_SHORT).show()
            }
        })
    }

    fun toggleDone() {
        var changedSale = Sale()
        changedSale.id = sale.id
        changedSale.client = sale.client.id
        changedSale.discount = sale.discount
        changedSale.cart = ArrayList()
        changedSale.cart.addAll(sale.cart)
        changedSale.total = sale.totalDouble
        changedSale.payment = sale.payment
        changedSale.created = sale.created
        changedSale.done = !sale.done
        changedSale.type = sale.type
        changedSale.print = sale.print
        changedSale.shift = sale.shift.id
        changedSale.subtotal = sale.subtotal
        v.progress.visibility = View.VISIBLE
        ServiceGenerator.createService(APIClient::class.java, token, activity).saveSale(sale.id, changedSale).enqueue(object : Callback<Sale> {

            override fun onResponse(call: Call<Sale>, response: Response<Sale>) {
                if (response.isSuccessful) {
                    responseCode = response.code()
                    sale.done = response.body()!!.done
                    v.progress.visibility = View.GONE
                }
            }

            override fun onFailure(call: Call<Sale>, t: Throwable) {
                v.progress.visibility = View.GONE
                Toast.makeText(activity, "Ошибка ${t.message}", Toast.LENGTH_SHORT).show()
            }
        })
    }

    fun loadSale() {
        ServiceGenerator.createService(APIClient::class.java, token, activity).getSale(saleId).enqueue(object : Callback<SaleHistory> {
            override fun onResponse(call: Call<SaleHistory>, response: Response<SaleHistory>) {
                if (response.isSuccessful) {
                    responseCode = response.code()
                    if (responseCode == 200) {
                        sale = response.body()!!
                        sale.edit = false
                        binding.sale = sale
                        if (sale.type == "+") {
                            dialog.setTitle("Продажа №${sale.id}")
                        } else {
                            dialog.setTitle("Возврат №${sale.id}")
                        }
                        var adapter = SaleProductsAdapter(activity, sale.cart, sale.client,false,this@SaleDialogFragment)
                        rv.adapter = adapter
                    }
                } else {
                    Toast.makeText(activity, activity.getString(R.string.sale_get_error), Toast.LENGTH_SHORT).show()
                }
                v.progress.visibility = View.GONE
            }

            override fun onFailure(call: Call<SaleHistory>, t: Throwable) {
                v.progress.visibility = View.GONE
                Toast.makeText(activity, t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
        var fragment = fragmentManager.findFragmentByTag("salesFragment")
        (fragment!! as SalesFragment).loadPage((fragment!! as SalesFragment).page)
    }

    override fun onCancel(dialog: DialogInterface?) {
        super.onCancel(dialog)
    }
}
