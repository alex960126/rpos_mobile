package uz.ds.coder.RPOS.Fragment


import android.app.AlertDialog
import android.app.Fragment
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_sales.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import uz.ds.coder.RPOS.Adapter.SalesAdapter
import uz.ds.coder.RPOS.Product.SalesRequest
import uz.ds.coder.RPOS.R
import uz.ds.coder.RPOS.Utils.APIClient
import uz.ds.coder.RPOS.Utils.ServiceGenerator


/**
 * A simple [Fragment] subclass.
 */
class SalesFragment : Fragment() {

    private lateinit var rv:RecyclerView
    private lateinit var pref:SharedPreferences
    private var token = ""
    private var responseCode = -1
    public var page = 1
    private var count = 0
    private var next_link: String? = null
    private var prev_link: String? = null
    private lateinit var current: TextView
    private lateinit var page_count: TextView
    private lateinit var dialog: AlertDialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var v = inflater.inflate(R.layout.fragment_sales, container, false)
        rv = v.findViewById(R.id.sales_rv)
        rv.layoutManager = LinearLayoutManager(activity)
        pref = PreferenceManager.getDefaultSharedPreferences(activity)

        v.first.setOnClickListener({
            loadPage(1)
        })
        v.last.setOnClickListener({
            loadPage((count + 9) / 10)
        })
        v.next.setOnClickListener({
            if (page < (count + 9) / 10)
                loadPage(page + 1)
        })
        v.prev.setOnClickListener({
            if (page > 1)
                loadPage(page - 1)
        })
        v.go_btn.setOnClickListener {
            var page_num = v.page_numb.text.toString().toInt()
            if (page_num >= 1 && page_num <= (count + 9) / 10 && page_num != page) {
                loadPage(page_num)
            }
        }
        current = v.current
        page_count = v.page_count
        token = pref.getString("TOKEN", "")
        loadSales()
        return v
    }

    fun loadSales() {
        loadPage(1)
    }

    fun loadPage(page_to_load: Int) {
        setProgressDialog()
        ServiceGenerator.createService(APIClient::class.java, token, activity).getListSales(page_to_load).enqueue(object : Callback<SalesRequest> {
            override fun onResponse(call: Call<SalesRequest>, response: Response<SalesRequest>) {
                if (response.isSuccessful) {
                    responseCode = response.code()
                    if (responseCode == 200) {
                        var adapter = SalesAdapter(activity, response.body()!!.results, this@SalesFragment)
                        rv.adapter = adapter
                        page = page_to_load
                        count = response.body()!!.count
                        current.text = page.toString()
                        var page_cnt = (count + 9) / 10
                        page_count.text = "$page_cnt страниц"
                    }
                } else {
                    Toast.makeText(activity, activity.getString(R.string.sale_get_error), Toast.LENGTH_SHORT).show()
                }
                dialog.dismiss()
            }

            override fun onFailure(call: Call<SalesRequest>, t: Throwable) {
                dialog.dismiss()
                Toast.makeText(activity, t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    fun setProgressDialog() {

        val llPadding = 30
        val ll = LinearLayout(activity)
        ll.orientation = LinearLayout.HORIZONTAL
        ll.setPadding(llPadding, llPadding, llPadding, llPadding)
        ll.gravity = Gravity.CENTER
        var llParam = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT)
        llParam.gravity = Gravity.CENTER
        ll.layoutParams = llParam

        val progressBar = ProgressBar(activity)
        progressBar.setIndeterminate(true)
        progressBar.setPadding(0, 0, llPadding, 0)
        progressBar.setLayoutParams(llParam)

        llParam = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT)
        llParam.gravity = Gravity.CENTER
        val tvText = TextView(activity)
        tvText.text = activity.getText(R.string.dialog_loading)
        tvText.setTextColor(Color.parseColor("#000000"))
        tvText.textSize = 20f
        tvText.layoutParams = llParam

        ll.addView(progressBar)
        ll.addView(tvText)

        val builder = AlertDialog.Builder(activity)
        builder.setCancelable(true)
        builder.setView(ll)

        dialog = builder.create()
        dialog.setCancelable(false)
        dialog.show()
        val window = dialog.getWindow()
        if (window != null) {
            val layoutParams = WindowManager.LayoutParams()
            layoutParams.copyFrom(dialog.getWindow().getAttributes())
            layoutParams.width = LinearLayout.LayoutParams.WRAP_CONTENT
            layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
            dialog.getWindow().setAttributes(layoutParams)
        }
    }

}
