
package uz.ds.coder.RPOS.Product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import uz.ds.coder.RPOS.models.SaleHistory;

public class SalesRequest {

    @SerializedName("links")
    @Expose
    private Links links;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("results")
    @Expose
    private List<SaleHistory> results = null;

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<SaleHistory> getResults() {
        return results;
    }

    public void setResults(List<SaleHistory> results) {
        this.results = results;
    }

}
