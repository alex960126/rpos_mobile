package uz.ds.coder.RPOS.Dialog;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uz.ds.coder.RPOS.Fragment.PosFragment;
import uz.ds.coder.RPOS.Product.Client;
import uz.ds.coder.RPOS.R;
import uz.ds.coder.RPOS.Utils.APIClient;
import uz.ds.coder.RPOS.Utils.ServiceGenerator;

/**
 * Created by Coder on 11-Mar-18.
 */

public class AddClientDialog extends DialogFragment implements View.OnClickListener {

    private EditText fullname;
    private EditText phone;
    private EditText email;
    private EditText birthday;
    private Spinner sexSpinner;
    private String mToken;
    private int responseCode = 0;
    private int type;

    public static AddClientDialog newInstance(String token, int type) {
        AddClientDialog f = new AddClientDialog();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("token", token);
        args.putInt("type", type);
        f.setArguments(args);

        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_client,container);
        mToken = getArguments().getString("token");
        type = getArguments().getInt("type");
        v.findViewById(R.id.close).setOnClickListener(this);
        v.findViewById(R.id.add).setOnClickListener(this);
        fullname = v.findViewById(R.id.name);
        phone = v.findViewById(R.id.phone);
        email = v.findViewById(R.id.email);
        birthday = v.findViewById(R.id.birthday);
        sexSpinner = v.findViewById(R.id.spinner);

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.close:
                this.dismiss();
                break;
            case R.id.add:
                Toast.makeText(getActivity(), "Added", Toast.LENGTH_SHORT).show();
                final Client client = new Client();
                client.setFullname(fullname.getText().toString());
                client.setPhone(phone.getText().toString());
                client.setEmail(email.getText().toString());
                client.setSex(sexSpinner.getSelectedItemPosition() == 0);
                if (((RadioButton)v.findViewById(R.id.type2)).isChecked()) {
                    type = 1;
                } else if (((RadioButton)v.findViewById(R.id.type3)).isChecked()) {
                    type = 2;
                }
                client.setType(type);

                ServiceGenerator.createService(APIClient.class, mToken, getActivity()).sendClient(client).enqueue(new Callback<Client>() {
                    @Override
                    public void onResponse(Call<Client> call, Response<Client> response) {
                        if (response.isSuccessful()) {
                            Client client = response.body();
                            Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                            if (type == 0) {
                                ((PosFragment)getParentFragment()).addClient(client);
                            } else {
                            }
                        }
                        AddClientDialog.this.dismiss();
                    }

                    @Override
                    public void onFailure(Call<Client> call, Throwable t) {
                        Toast.makeText(getActivity(), "Error adding client", Toast.LENGTH_SHORT).show();
                    }
                });
                break;
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }
}