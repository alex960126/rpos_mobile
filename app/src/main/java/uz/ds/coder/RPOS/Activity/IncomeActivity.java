package uz.ds.coder.RPOS.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uz.ds.coder.RPOS.Adapter.IncomeCartAdapter;
import uz.ds.coder.RPOS.Adapter.ClientSpinnerAdapter;
import uz.ds.coder.RPOS.Adapter.IncomeProductsAdapter;
import uz.ds.coder.RPOS.Dialog.AddClientDialog;
import uz.ds.coder.RPOS.Dialog.LogoutRequestDialog;
import uz.ds.coder.RPOS.Product.Client;
import uz.ds.coder.RPOS.Product.Group;
import uz.ds.coder.RPOS.Product.GroupRequest;
import uz.ds.coder.RPOS.Product.Product;
import uz.ds.coder.RPOS.Product.Sale;
import uz.ds.coder.RPOS.Product.Shift;
import uz.ds.coder.RPOS.R;
import uz.ds.coder.RPOS.Utils.APIClient;
import uz.ds.coder.RPOS.Utils.RequestCodes;
import uz.ds.coder.RPOS.Utils.ServiceGenerator;
import uz.ds.coder.RPOS.databinding.DialogPaymentBinding;
import uz.ds.coder.RPOS.models.Payment;

public class IncomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {

    private int responseCode = 0;
    private RecyclerView recyclerView;
    private RecyclerView cartRecyclerView;

    private IncomeProductsAdapter adapter;
    private IncomeCartAdapter cartAdapter;
    private SharedPreferences pref;
    public String token = "";
    ImageButton adduser;
    String title;
    private Spinner clientSpinner;
    public static List<Product> cart;
    public static Double total = 0.0;
    public static Integer discount = 0;
    public static TextView tv_total;
    public static TextView tv_discount;
    public static TextView tv_subtotal;
    public static TextView tv_sum;
    private ImageButton clearCartButton;
    public static EditText product_search;
    private Client client = null;
    AddClientDialog dialog;
    private View pay;
    private Shift shift;
    private RadioGroup rg;



    List<Product> products;
    List<Client> clients;
    private Payment payment;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        payment = new Payment();


        setContentView(R.layout.activity_income);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = getResources().getString(R.string.app_name);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Приход");

        pref = PreferenceManager.getDefaultSharedPreferences(this);
        token = pref.getString("TOKEN", "");
        products = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.list);
        cartRecyclerView = (RecyclerView) findViewById(R.id.cart_recycler);
        clearCartButton = findViewById(R.id.clearCart);
        product_search = findViewById(R.id.product_search);
        dialog = AddClientDialog.newInstance(token, 0);

        pay = findViewById(R.id.pay);
        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openPaymentDialog();
            }
        });
        shift = new Shift();
        rg = findViewById(R.id.products_group);

        findViewById(R.id.discount_holder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });



        if (token == "") {
            Intent logIntent = new Intent(this, LoginActivity.class);
            startActivityForResult(logIntent, RequestCodes.REQUEST_LOGIN);
        } else {
            LoadDataFromServer();
        }

        clientSpinner = findViewById(R.id.buyer);
        tv_total = (TextView)findViewById(R.id.tv_total);
        tv_discount = findViewById(R.id.tv_discount);
        tv_discount.setText(discount.toString());
        tv_subtotal = findViewById(R.id.tv_subtotal);
        tv_sum = findViewById(R.id.sum);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    public void GenerateRadioGroups(GroupRequest groupRequest) {
        int i = 1;
        RadioButton radio = new RadioButton(this);
        radio.setId(0);
        radio.setText("Все");
        radio.setChecked(true);
        radio.setPadding(10, 10, 10, 10);
        radio.setBackground(getDrawable(R.drawable.rbtn_selector));
        radio.setButtonDrawable(android.R.color.transparent);
        radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.getFilter().filter("");
            }
        });
        rg.addView(radio);
        for(Group gr : groupRequest.getResults()) {

            radio = new RadioButton(this);
            final String title = gr.getTitle();
            radio.setText(gr.getTitle());
            radio.setId(i++);
            radio.setBackground(getDrawable(R.drawable.rbtn_selector));
            radio.setButtonDrawable(android.R.color.transparent);
            radio.setPadding(10, 10, 10, 10);
            radio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adapter.getFilter().filter(title);
                }
            });
            rg.addView(radio);
        }
    }


    public void LoadDataFromServer() {
        ServiceGenerator.createService(APIClient.class,token,this).getlistProducts().
                enqueue(new Callback<List<Product>>() {
                    @Override
                    public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                        if (response.isSuccessful()) {
                            responseCode = response.code();
                            if (responseCode==200) {
                                products.addAll(response.body());
                                GridLayoutManager layoutManager = new GridLayoutManager(IncomeActivity.this, 4);
                                recyclerView.setLayoutManager(layoutManager);
                                adapter = new IncomeProductsAdapter(products, IncomeActivity.this);
                                recyclerView.setAdapter(adapter);
                                recyclerView.setHasFixedSize(true);
                                recyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
                                recyclerView.getRecycledViewPool().setMaxRecycledViews(0, 0);

                                cartRecyclerView.setHasFixedSize(true);
                                cartRecyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
                                cartRecyclerView.setLayoutManager(new LinearLayoutManager(IncomeActivity.this, LinearLayoutManager.VERTICAL,false));
                                cartRecyclerView.getRecycledViewPool().setMaxRecycledViews(0, 0);

                                cartAdapter = new IncomeCartAdapter(IncomeActivity.this, IncomeProductsAdapter.selectedProducts);
                                cartRecyclerView.setAdapter(cartAdapter);

                                product_search.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {
                                        adapter.getFilter().filter(s.toString());
                                    }
                                });


                                clearCartButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        IncomeProductsAdapter.selectedProducts.clear();
                                        payment = new Payment();
                                        cartAdapter.notifyDataSetChanged();
                                        calculateTotal();
                                    }
                                });

                                calculateTotal();


                                cart = new ArrayList<>();
                            } else {
                                Toast.makeText(IncomeActivity.this, "Данных нет", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(IncomeActivity.this,"response.code() != 200",Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Product>> call, Throwable t) {
                        Toast.makeText(IncomeActivity.this, t.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                });
        ServiceGenerator.createService(APIClient.class,token, this).getlistClients().enqueue(new Callback<List<Client>>() {
            @Override
            public void onResponse(Call<List<Client>> call, Response<List<Client>> response) {
                if (response.isSuccessful()) {
                    responseCode = response.code();
                    if (responseCode == 200) {
                        clients = new ArrayList<>();
                        clients.addAll(response.body());

                        if (client == null) {
                            client = clients.get(0);
                        }
                        ClientSpinnerAdapter adapter = new ClientSpinnerAdapter(IncomeActivity.this, android.R.layout.simple_spinner_dropdown_item, android.R.id.text1, clients);
                        clientSpinner.setAdapter(adapter);
                        clientSpinner.setSelection(0);
                        clientSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                client = clients.get(position);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                client = clients.get(0);
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Client>> call, Throwable t) {
                Toast.makeText(IncomeActivity.this, "Error getting clients", Toast.LENGTH_SHORT).show();
            }
        });
        ServiceGenerator.createService(APIClient.class,token, this).getCurrentShift().enqueue(new Callback<Shift>() {
            @Override
            public void onResponse(Call<Shift> call, Response<Shift> response) {
                if (response.isSuccessful()) {
                    responseCode = response.code();
                    if (responseCode == 200) {
                        shift = response.body();
                    }
                }
            }

            @Override
            public void onFailure(Call<Shift> call, Throwable t) {
                Toast.makeText(IncomeActivity.this, "Error getting clients", Toast.LENGTH_SHORT).show();
            }
        });
        ServiceGenerator.createService(APIClient.class,token, this).getGroups().enqueue(new Callback<GroupRequest>() {
            @Override
            public void onResponse(Call<GroupRequest> call, Response<GroupRequest> response) {
                if (response.isSuccessful()) {
                    responseCode = response.code();
                    if (responseCode == 200) {
                        GenerateRadioGroups(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<GroupRequest> call, Throwable t) {
                Toast.makeText(IncomeActivity.this, "Error getting clients", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void addClient(Client client) {
        clients.add(client);
        clientSpinner.setSelection(clients.size() - 1);
    }

    public void addProductToCart() {
        cartAdapter.notifyDataSetChanged();
        calculateTotal();
    }

    public void deleteProductFromCart(Product product) {
        IncomeProductsAdapter.selectedProducts.remove(product);
        cartAdapter.notifyDataSetChanged();
        calculateTotal();
    }

    public void clearCart() {
        IncomeProductsAdapter.selectedProducts.clear();
        cartAdapter.notifyDataSetChanged();
        calculateTotal();
    }

    public void refreshCart() {
        cartAdapter.notifyDataSetChanged();
        calculateTotal();
    }



    public static void calculateTotal(){
        int i=0;
        total=0.0;
        while(i<IncomeProductsAdapter.selectedProducts.size()){
            total=total + Double.valueOf(IncomeProductsAdapter.selectedProducts.get(i).getPrice()) * Double.valueOf(IncomeProductsAdapter.selectedProducts.get(i).getCount());
            i++;
        }
        tv_subtotal.setText(NumberFormat.getNumberInstance(Locale.getDefault()).format(total));
        tv_total.setText(NumberFormat.getNumberInstance(Locale.getDefault()).format(total * (1 - Double.valueOf(discount) / 100.0)));
        tv_sum.setText(tv_total.getText());
    }

    public void logoutclick(View view) {
        navigationView.getHeaderView(0).findViewById(R.id.logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogoutRequestDialog dlg = new LogoutRequestDialog();
                dlg.show(getFragmentManager(), "logoutRequest");
            }
        });
    }

    public void addUser(View view) {
        dialog = AddClientDialog.newInstance(token, 0);
        dialog.show(getFragmentManager(),"Adding_user");
    }

    /**
     * public void scanCode() {
     Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
     File photo = new File(Environment.getExternalStorageDirectory(), "pic.jpg");
     intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
     Uri imageUri = Uri.fromFile(photo);
     startActivityForResult(intent, 1);
     }
     */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode!=RequestCodes.LOGIN_SUCCESSFULL) {
            Toast.makeText(this, "not logged",Toast.LENGTH_SHORT).show();
        } else {
            LoadDataFromServer();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        final MenuItem searchItem = menu.findItem(R.id.search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar cart_item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id==R.id.action_settings) {
            Toast.makeText(this,"clicked Settings",Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view cart_item clicks here.
        int id = item.getItemId();

        if (id == R.id.income) {
            title = "Income";
            getSupportActionBar().setTitle(title);
            Intent intent = new Intent(this, IncomeActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void openPaymentDialog(){
        final DialogPaymentBinding binding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.dialog_payment, null, false);
        payment.setAmount(total - (1 * discount / 100));
        payment.setToPay(total - (1 * discount / 100));
        binding.setPayment(payment);
        View subView = binding.getRoot();

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(String.format("Платеж"));
        builder.setMessage(String.format("Добавить платеж"));
        builder.setView(subView);
        Button btn = subView.findViewById(R.id.payment_done_btn);
        final View addPaymentHolder = subView.findViewById(R.id.add_payment_holder);
        final View closePaymentHolder = subView.findViewById(R.id.close_payment_holder);
        final RadioButton cashRadio = subView.findViewById(R.id.radio_cash);
        final EditText amountET = subView.findViewById(R.id.amount);
        amountET.selectAll();
        final Button closeBtn = subView.findViewById(R.id.payment_close_btn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double amount = 0;
                String txt_value = amountET.getText().toString();
                if (!txt_value.isEmpty()) {
                    try {
                        amount = Double.parseDouble(txt_value);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                if (cashRadio.isChecked()) {
                    payment.setCash(payment.getCash() + amount);
                } else {
                    payment.setCard(payment.getCard() + amount);
                }
                binding.executePendingBindings();
                binding.setPayment(payment);
                if (payment.getCash() + payment.getCard() >= total) {
                    addPaymentHolder.setVisibility(View.GONE);
                    closePaymentHolder.setVisibility(View.VISIBLE);
                    SendSale("-", closeBtn);
                }
            }
        });


        final AlertDialog dialog = builder.show();
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                clearCartButton.performClick();
            }
        });
    }

    public void SendSale(String type, final Button btn) {
        Sale sale = new Sale();
        sale.setCart(adapter.selectedProducts);
        uz.ds.coder.RPOS.Product.Payment pm = new uz.ds.coder.RPOS.Product.Payment();
        pm.setAmount(payment.getAmount().doubleValue());
        pm.setCard(payment.getCard().doubleValue());
        pm.setCash(payment.getCash().doubleValue());
        pm.setToPay(payment.getToPay().doubleValue());
        sale.setPayment(pm);
        sale.setType(type);
        sale.setPrint(false);
        sale.setTotal(total * (1 - Double.valueOf(discount) / 100.0));
        sale.setSubtotal(total);
        sale.setDiscount(Double.parseDouble(discount.toString()));
        sale.setClient(client.getId());
        sale.setShift(shift.getId());
        Gson gson = new Gson();
        Log.e("RETROFIT2", gson.toJson(sale));
        /*ServiceGenerator.createService(APIClient.class, token, IncomeActivity.this).sendIncome(sale).enqueue(new Callback<Sale>(){

            @Override
            public void onResponse(Call<Sale> call, Response<Sale> response) {
                if (response.isSuccessful()) {
                    responseCode = response.code();
                    if (responseCode == 200) {
                        Toast.makeText(IncomeActivity.this, "Успешно", Toast.LENGTH_SHORT).show();
                        btn.setEnabled(true);
                    }
                }
            }

            @Override
            public void onFailure(Call<Sale> call, Throwable t) {
                Log.e("RETROFIT2 ERROR",t.getMessage());
                Toast.makeText(IncomeActivity.this, "Ошибка", Toast.LENGTH_SHORT).show();
                btn.setEnabled(true);
            }
        });*/
    }
    public void ChangeDiscountDialog(View view) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View subView = inflater.inflate(R.layout.change_price, null);
        final EditText text = subView.findViewById(R.id.price);
        text.setText(discount.toString());
        text.selectAll();


        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(String.format("Скидка"));
        builder.setMessage(String.format("Сделать скидку"));
        builder.setView(subView);
        builder.setPositiveButton("Сохранить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String txt_value = text.getText().toString();
                if (!txt_value.isEmpty()) {
                    try {
                        discount = Double.valueOf(txt_value).intValue();
                        tv_discount.setText(IncomeActivity.discount.toString());
                        calculateTotal();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }

            }
        });

        builder.setNegativeButton("Отменить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(IncomeActivity.this, "Отмена", Toast.LENGTH_LONG).show();
            }
        });


        final AlertDialog dialog = builder.show();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        return true;
    }
}

