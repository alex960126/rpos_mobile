package uz.ds.coder.RPOS.Product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Shift {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("opened")
    @Expose
    private String opened;
    @SerializedName("closed")
    @Expose
    private String closed;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("startcash")
    @Expose
    private Double startcash;
    @SerializedName("closecash")
    @Expose
    private Double closecash;
    @SerializedName("shop")
    @Expose
    private Integer shop;
    @SerializedName("employee")
    @Expose
    private Integer employee;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOpened() {
        return opened;
    }

    public void setOpened(String opened) {
        this.opened = opened;
    }

    public String getClosed() {
        return closed;
    }

    public void setClosed(String closed) {
        this.closed = closed;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Double getStartcash() {
        return startcash;
    }

    public void setStartcash(Double startcash) {
        this.startcash = startcash;
    }

    public Object getClosecash() {
        return closecash;
    }

    public void setClosecash(Double closecash) {
        this.closecash = closecash;
    }

    public Integer getShop() {
        return shop;
    }

    public void setShop(Integer shop) {
        this.shop = shop;
    }

    public Integer getEmployee() {
        return employee;
    }

    public void setEmployee(Integer employee) {
        this.employee = employee;
    }

}