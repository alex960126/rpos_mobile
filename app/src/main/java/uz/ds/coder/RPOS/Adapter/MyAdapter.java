package uz.ds.coder.RPOS.Adapter;

import android.app.Fragment;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import uz.ds.coder.RPOS.Fragment.IncomeFragment;
import uz.ds.coder.RPOS.Fragment.PosFragment;
import uz.ds.coder.RPOS.Product.Product;
import uz.ds.coder.RPOS.R;
import uz.ds.coder.RPOS.Utils.Constants;
import uz.ds.coder.RPOS.Utils.TextFormatters;

/**
 * Created by Coder on 06-Mar-18.
 */



public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private List<Product> products;
    private Context context;
    public static List<Product> selectedProducts;
    public static int client_type = 0;
    final List<Product> templist=new ArrayList<>();
    private Fragment fragment;
    private int type;

    public MyAdapter(List<Product> products, Context context, Fragment fragment, int type) {
        this.products = products;
        this.context = context;
        selectedProducts = new ArrayList<>();
        templist.addAll(products);
        this.fragment = fragment;
        this.type = type;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_item, parent, false);
        return new ViewHolder(view);
    }

    public void init(){
        templist.addAll(products);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Product product = products.get(position);
        holder.title.setText(product.getTitle());
        final double price =
                (client_type == 0) ? product.getPrice() :
                (client_type == 1) ? product.getPricesrd() : product.getPriceopt();
        holder.price.setText(TextFormatters.priceFormatter().format(price));
        Glide.with(context)
                .load(Constants.SERVER_URL.substring(0, Constants.SERVER_URL.length() - 1) + product.getImage())
                .into(holder.image);

        holder.mView.setOnClickListener(view -> {
        if (selectedProducts.contains(product)) {
            Product product1 = selectedProducts.get(selectedProducts.indexOf(product));
            selectedProducts.get(selectedProducts.indexOf(product)).setTotal((product1.getCount() + 1) * price * (1 - ((PosFragment)fragment).discount / 100));
            selectedProducts.get(selectedProducts.indexOf(product)).setCount(product1.getCount() + 1);

        } else {
            int discount = (type == 0) ? ((PosFragment)fragment).discount : ((IncomeFragment)fragment).discount;
            product.setDiscount(discount);
            product.setCount(1.0);
            product.setContainer(false);
            product.setTotal(product.getCount() * price * (1 - discount / 100));
            selectedProducts.add(product);
        }
        if (type == 0) {
            ((PosFragment)fragment).addProductToCart();
        } else {
            ((IncomeFragment)fragment).addProductToCart();
        }
        });

    }

    public Filter getFilter() {

        Filter mfilter = new Filter() {
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                products.clear();
                products.addAll((ArrayList<Product>)results.values);
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults result = new FilterResults();
                ArrayList<Product> res;

                res = new ArrayList<>();
                if(constraint.length()>0){
                    for(Product d: templist){
                        Log.e("TAG", d.getGroup().getTitle());
                        boolean isTitle = (d.getTitle() != null) && d.getTitle().toUpperCase().contains(constraint.toString().toUpperCase());
                        boolean isArticle = (d.getArticle() != null) && d.getArticle().toUpperCase().contains(constraint.toString().toUpperCase());
                        boolean isBarcode = (d.getBarcode() != null) && d.getBarcode().toUpperCase().contains(constraint.toString().toUpperCase());
                        boolean isGroup = (d.getGroup() != null) && d.getGroup().getTitle().toUpperCase().contains(constraint.toString().toUpperCase());
                        if(isTitle || isArticle || isBarcode || isGroup) {
                            res.add(d);
                        }
                    }
                    result.count = res.size();
                    result.values = res;
                }

                if(constraint.length() == 0){
                    res.addAll(templist);
                    result.count = res.size();
                    result.values = res;
                }

                return result;
            }
        };

        return mfilter;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return products.size();
    }

    public void changeProduct(List<Product> products) {
        this.products = products;
    }

    // Provide a reference to the views for each data cart_item
    // Complex data items may need more than one view per cart_item, and
    // you provide access to all the views for a data cart_item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data cart_item is just a string in this case
        View mView;
        TextView title;
        TextView price;
        ImageView image;
        public ViewHolder(View v) {
            super(v);
            mView = v.findViewById(R.id.content_holder);
            title = (TextView) v.findViewById(R.id.title);
            price = (TextView) v.findViewById(R.id.price);
            image = (ImageView) v.findViewById(R.id.image);
        }
    }
}
