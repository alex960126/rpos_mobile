package uz.ds.coder.RPOS.Adapter;

import android.app.Fragment;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import uz.ds.coder.RPOS.Fragment.SaleDialogFragment;
import uz.ds.coder.RPOS.Product.Product;
import uz.ds.coder.RPOS.R;
import uz.ds.coder.RPOS.models.SaleHistory;

import static uz.ds.coder.RPOS.Utils.TextFormatters.priceFormatter;

/**
 * Created by Coder on 14-Mar-18.
 */

public class SalesAdapter extends RecyclerView.Adapter<SalesAdapter.ViewHolder> {
    private List<SaleHistory> sales;
    private Context context;
    private Fragment fragment;


    public SalesAdapter(Context context, List<SaleHistory> sales, Fragment fragment)  {
        this.sales = sales;
        this.context = context;
        this.fragment = fragment;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SalesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sales_item, parent, false);
        return new SalesAdapter.ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(SalesAdapter.ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final SaleHistory sale = sales.get(position);
        holder.id.setText(sale.getId()+"");

        holder.client.setText(sale.getClient().getFullname());
        holder.count.setText(sale.getCart().size()+"");
        holder.created.setText(sale.getCreated());
        holder.payment.setText(priceFormatter().format(sale.getPayment().getCardDouble() + sale.getPayment().getCashDouble()));
        holder.status.setText((sale.getDone()) ? context.getString(R.string.done) : context.getString(R.string.not_done));
        holder.viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaleDialogFragment dialog = SaleDialogFragment.Companion.newInstance(sale.getId());
                dialog.show(fragment.getFragmentManager(), "saleDialogFragment");
            }
        });
    }

    @Override
    public int getItemCount() {
        return sales.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data cart_item is just a string in this case
        View mView;
        TextView id;
        TextView client;
        TextView created;
        TextView count;
        TextView status;
        TextView payment;
        ImageButton viewButton;
        public ViewHolder(View v) {
            super(v);
            mView = v.findViewById(R.id.content_holder);
            id = v.findViewById(R.id.id);
            client = v.findViewById(R.id.client);
            created = v.findViewById(R.id.date);
            count = v.findViewById(R.id.count);
            status = v.findViewById(R.id.status);
            payment = v.findViewById(R.id.payment);
            viewButton = v.findViewById(R.id.viewBtn);
        }
    }

    String[] dialogTitles = new String[]{"цену", "количество"};

    private void openDialog(final Product sale, final int type){
        LayoutInflater inflater = LayoutInflater.from(context);
        View subView = inflater.inflate(R.layout.change_price, null);
        final EditText subEditText = subView.findViewById(R.id.price);
        subEditText.setText((type == 0) ? sale.getPrice().toString() : sale.getCount().toString());
        subEditText.selectAll();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(String.format("Изменить %s", dialogTitles[type]));
        builder.setMessage(String.format("Введите %s", dialogTitles[type]));
        builder.setView(subView);
        builder.create();

        builder.setPositiveButton("Сохранить", (dialog, which) -> {
            if (type == 0) {
                sale.setPrice(Double.valueOf(subEditText.getText().toString()));
            } else {
                sale.setCount(Double.valueOf(subEditText.getText().toString()));
            }
            sale.setTotal(sale.getCount() * sale.getPrice() * (1 - sale.getDiscount() / 100));

        });

        builder.setNegativeButton("Отменить", (dialog, which) -> Toast.makeText(context, "Cancel", Toast.LENGTH_LONG).show());

        builder.show();
    }

}

