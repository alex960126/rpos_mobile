
package uz.ds.coder.RPOS.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import uz.ds.coder.RPOS.Product.Payment;


public class SaleForSave {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("cart")
    @Expose
    private List<CartProduct> cart = null;
    @SerializedName("payment")
    @Expose
    private Payment payment;
    @SerializedName("client")
    @Expose
    private Integer client;
    @SerializedName("agent")
    @Expose
    private Integer agent;

    public Integer getAgent() {
        return agent;
    }

    public void setAgent(Integer agent) {
        this.agent = agent;
    }

    @SerializedName("shift")
    @Expose
    private Integer shift;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("print")
    @Expose
    private Boolean print;
    @SerializedName("total")
    @Expose
    private Double total;
    @SerializedName("subtotal")
    @Expose
    private Double subtotal;

    @SerializedName("done")
    @Expose
    private Boolean done;
    @SerializedName("discount")
    @Expose
    private Double discount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<CartProduct> getCart() {
        return cart;
    }

    public void setCart(List<CartProduct> cart) {
        this.cart = cart;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Integer getClient() {
        return client;
    }

    public void setClient(Integer client) {
        this.client = client;
    }

    public Integer getShift() {
        return shift;
    }

    public void setShift(Integer shift) {
        this.shift = shift;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getPrint() {
        return print;
    }

    public void setPrint(Boolean print) {
        this.print = print;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

}
