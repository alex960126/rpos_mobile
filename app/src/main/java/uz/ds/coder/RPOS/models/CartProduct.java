package uz.ds.coder.RPOS.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import uz.ds.coder.RPOS.Product.Amount;
import uz.ds.coder.RPOS.Product.Group;

public class CartProduct {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("group")
    @Expose
    private Group group;
    @SerializedName("amounts")
    @Expose
    private List<Amount> amounts = null;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("pricesrd")
    @Expose
    private Double pricesrd;
    @SerializedName("priceopt")
    @Expose
    private Double priceopt;

    public Double getPricesrd() {
        return pricesrd;
    }

    public void setPricesrd(Double pricesrd) {
        this.pricesrd = pricesrd;
    }

    public Double getPriceopt() {
        return priceopt;
    }

    public void setPriceopt(Double priceopt) {
        this.priceopt = priceopt;
    }

    @SerializedName("barcode")
    @Expose
    private String barcode;
    @SerializedName("article")
    @Expose
    private String article;
    @SerializedName("buyprice")
    @Expose
    private Double buyprice;
    @SerializedName("length")
    @Expose
    private Double length;
    @SerializedName("width")
    @Expose
    private Double width;

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    @SerializedName("category")
    @Expose
    private List<String> category = null;
    @SerializedName("metrics")
    @Expose
    private String metrics;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("incontainer")
    @Expose
    private Integer incontainer;


    @SerializedName("is_container")
    @Expose
    private Boolean isContainer;

    public Boolean getContainer() {
        return isContainer;
    }

    public void setContainer(Boolean container) {
        isContainer = container;
    }

    public Integer getIncontainer() {
        if (this.incontainer == null) {
            return 1;
        }
        return incontainer;
    }

    public void setIncontainer(Integer incontainer) {
        this.incontainer = incontainer;
    }

    @SerializedName("count")
    @Expose
    private Double count;
    @SerializedName("total")
    @Expose
    private Double total;

    public Double getCount() {
        return count;
    }

    public void setCount(Double count) {
        this.count = count;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    @SerializedName("expirydate")
    @Expose
    private String expirydate;
    @SerializedName("minbalance")
    @Expose
    private Long minbalance;
    @SerializedName("created")
    @Expose
    private String created;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public List<Amount> getAmounts() {
        return amounts;
    }

    public void setAmounts(List<Amount> amounts) {
        this.amounts = amounts;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public Double getBuyprice() {
        return buyprice;
    }

    public void setBuyprice(Double buyprice) {
        this.buyprice = buyprice;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public List<String> getCategory() {
        return category;
    }

    public void setCategory(List<String> category) {
        this.category = category;
    }

    public String getMetrics() {
        return metrics;
    }

    public void setMetrics(String metrics) {
        this.metrics = metrics;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(String expirydate) {
        this.expirydate = expirydate;
    }

    public Long getMinbalance() {
        return minbalance;
    }

    public void setMinbalance(Long minbalance) {
        this.minbalance = minbalance;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public boolean isContainer() {
        return isContainer;
    }

    public void setContainer(boolean container) {
        isContainer = container;
    }
}
