package uz.ds.coder.RPOS.Fragment;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.jetbrains.annotations.Nullable;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uz.ds.coder.RPOS.Adapter.Adapter;
import uz.ds.coder.RPOS.Adapter.ClientSpinnerAdapter;
import uz.ds.coder.RPOS.Adapter.ClientsListAdapter;
import uz.ds.coder.RPOS.Adapter.MyAdapter;
import uz.ds.coder.RPOS.Dialog.AddClientDialog;
import uz.ds.coder.RPOS.Product.Client;
import uz.ds.coder.RPOS.Product.Group;
import uz.ds.coder.RPOS.Product.GroupRequest;
import uz.ds.coder.RPOS.Product.Product;
import uz.ds.coder.RPOS.Product.Sale;
import uz.ds.coder.RPOS.Product.Shift;
import uz.ds.coder.RPOS.R;
import uz.ds.coder.RPOS.Utils.APIClient;
import uz.ds.coder.RPOS.Utils.ServiceGenerator;
import uz.ds.coder.RPOS.databinding.DialogPaymentBinding;
import uz.ds.coder.RPOS.databinding.FragmentPosBinding;
import uz.ds.coder.RPOS.models.Payment;

public class PosFragment extends Fragment implements SearchView.OnQueryTextListener {

    private MyAdapter adapter;
    private Adapter cartAdapter;
    private int responseCode = 0;
    private RecyclerView recyclerView;
    private RecyclerView cartRecyclerView;


    private SharedPreferences pref;
    public String token = "";
    ImageButton adduser;
    private Spinner clientSpinner;
    public static List<Product> cart;
    public static Double total = 0.0;
    public static Integer discount = 0;
    public static TextView tv_total;
    public static TextView tv_discount;
    public static TextView tv_subtotal;
    public static TextView tv_sum;
    private ImageButton clearCartButton;
    public static EditText product_search;
    private Client client = null;
    AddClientDialog dialog;
    private View pay;
    public Shift shift;
    private RadioGroup rg;
    private TextView tv_client;
    private Button btn_openshift;
    public View shiftBlocker;

    List<Product> products;
    List<Client> clients;
    private Payment payment;
    FragmentPosBinding binding;

    public PosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        //View v = inflater.inflate(R.layout.fragment_pos, container, false);

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pos, container, false);

        View v = binding.getRoot();
        client = new Client();
        binding.setClient(client);
        pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        token = pref.getString("TOKEN", "");
        products = new ArrayList<>();
        payment = new Payment();
        recyclerView = v.findViewById(R.id.list);
        cartRecyclerView = v.findViewById(R.id.cart_recycler);
        clearCartButton = v.findViewById(R.id.clearCart);
        product_search = v.findViewById(R.id.product_search);
        btn_openshift = v.findViewById(R.id.shift_open_btn);
        shiftBlocker = v.findViewById(R.id.shift_blocker);
        dialog = AddClientDialog.newInstance(token, 0);
        pay = v.findViewById(R.id.pay);
        v.findViewById(R.id.discount_change).setOnClickListener(this::ChangeDiscountDialog);

        v.findViewById(R.id.add_user).setOnClickListener(v12 -> addClientDialog());
        pay.setOnClickListener(view -> openPaymentDialog());
        shift = new Shift();
        rg = v.findViewById(R.id.products_group);

        v.findViewById(R.id.discount_holder).setOnClickListener(v13 -> {

        });
        btn_openshift.setOnClickListener(view -> openShift());


        clientSpinner = v.findViewById(R.id.buyer);
        tv_total = v.findViewById(R.id.tv_total);
        tv_discount = v.findViewById(R.id.tv_discount);
        tv_discount.setText(discount.toString());
        tv_subtotal = v.findViewById(R.id.tv_subtotal);
        tv_sum = v.findViewById(R.id.sum);
        tv_client = v.findViewById(R.id.client);
        LoadDataFromServer();

        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void convertContainer(Product product) {
        Product product1 = MyAdapter.selectedProducts.get(MyAdapter.selectedProducts.indexOf(product));

        if (product1.getIncontainer() > 0) {
            if (product1.getContainer()) {
                product1.setCount(product1.getCount() / product1.getIncontainer());
                product1.setContainer(false);
            } else {
                product1.setCount(product1.getCount() * product1.getIncontainer());
                product1.setContainer(true);
            }
        }
        cartAdapter.notifyDataSetChanged();
        calculateTotal();
    }

    public void openShift() {
        Shift mShift = new Shift();
        mShift.setEmployee(0);
        mShift.setStartcash(0.0);
        ServiceGenerator.createService(APIClient.class, token, getActivity()).openShift(mShift).enqueue(new Callback<Shift>() {

            @Override
            public void onResponse(@NonNull Call<Shift> call, @NonNull Response<Shift> response) {
                if (response.isSuccessful()) {
                    responseCode = response.code();
                    if (responseCode == 200) {
                        shift = response.body();
                        shiftBlocker.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Shift> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "Ошибка", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void addClient(Client client) {
        clients.add(client);
        clientSpinner.setSelection(clients.size() - 1);

    }

    public void deleteProductFromCart(Product product) {
        MyAdapter.selectedProducts.remove(product);
        cartAdapter.notifyDataSetChanged();
        calculateTotal();
    }

    public void refreshCart() {
        cartAdapter.notifyDataSetChanged();
        calculateTotal();
    }

    public void GenerateRadioGroups(GroupRequest groupRequest) {
        int i = 1;
        RadioButton radio = new RadioButton(getActivity());
        radio.setId(0);
        radio.setText("Все");
        radio.setChecked(true);
        radio.setPadding(10, 10, 10, 10);
        radio.setBackground(getActivity().getResources().getDrawable(R.drawable.rbtn_selector));
        radio.setButtonDrawable(android.R.color.transparent);
        radio.setOnClickListener(v -> adapter.getFilter().filter(""));
        rg.addView(radio);
        for (Group gr : groupRequest.getResults()) {

            radio = new RadioButton(getActivity());
            final String title = gr.getTitle();
            radio.setText(gr.getTitle());
            radio.setId(i++);
            radio.setBackground(getActivity().getResources().getDrawable(R.drawable.rbtn_selector));
            radio.setButtonDrawable(android.R.color.transparent);
            radio.setPadding(10, 10, 10, 10);
            radio.setOnClickListener(v -> adapter.getFilter().filter(title));
            rg.addView(radio);
        }
    }

    public void LoadDataFromServer() {
        ServiceGenerator.createService(APIClient.class, token, getActivity()).getlistProducts().
                enqueue(new Callback<List<Product>>() {
                    @Override
                    public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                        if (response.isSuccessful()) {
                            responseCode = response.code();
                            if (responseCode == 200) {
                                products.addAll(response.body());
                                GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 4);
                                recyclerView.setLayoutManager(layoutManager);
                                adapter = new MyAdapter(products, getActivity(), PosFragment.this, 0);
                                recyclerView.setAdapter(adapter);
                                recyclerView.setHasFixedSize(true);
                                recyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
                                recyclerView.getRecycledViewPool().setMaxRecycledViews(0, 0);

                                cartRecyclerView.setHasFixedSize(true);
                                cartRecyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
                                cartRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                                cartRecyclerView.getRecycledViewPool().setMaxRecycledViews(0, 0);

                                cartAdapter = new Adapter(getActivity(), MyAdapter.selectedProducts, PosFragment.this, 0, 0);
                                cartRecyclerView.setAdapter(cartAdapter);

                                product_search.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {
                                        adapter.getFilter().filter(s.toString());
                                    }
                                });


                                clearCartButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        MyAdapter.selectedProducts.clear();
                                        payment = new Payment();
                                        cartAdapter.notifyDataSetChanged();
                                        calculateTotal();
                                    }
                                });

                                calculateTotal();


                                cart = new ArrayList<>();
                            } else {
                                Toast.makeText(getActivity(), "Данных нет", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "response.code() != 200", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<List<Product>> call, @NonNull Throwable t) {
                        Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        ServiceGenerator.createService(APIClient.class, token, getActivity()).getlistClients().enqueue(new Callback<List<Client>>() {
            @Override
            public void onResponse(@NonNull Call<List<Client>> call, @NonNull Response<List<Client>> response) {
                if (response.isSuccessful()) {
                    responseCode = response.code();
                    if (responseCode == 200) {
                        clients = new ArrayList<>();
                        assert response.body() != null;
                        clients.addAll(Objects.requireNonNull(response.body()));

                        client = clients.get(0);
                        tv_client.setText(client.getFullname());

                        if (getActivity() != null) {
                            tv_client.setOnClickListener(view -> openClientChooseDialog());
                            ClientSpinnerAdapter adapter = new ClientSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, android.R.id.text1, clients);
                            clientSpinner.setAdapter(adapter);
                            clientSpinner.setSelection(0);
                            clientSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    client = clients.get(position);
                                    tv_client.setText(client.getFullname());
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    client = clients.get(0);
                                    tv_client.setText(client.getFullname());
                                }
                            });

                        }

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Client>> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "Error getting clients", Toast.LENGTH_SHORT).show();
            }
        });
        ServiceGenerator.createService(APIClient.class, token, getActivity()).getCurrentShift().enqueue(new Callback<Shift>() {
            @Override
            public void onResponse(@NonNull Call<Shift> call, @NonNull Response<Shift> response) {
                if (response.isSuccessful()) {
                    responseCode = response.code();
                    if (responseCode == 200) {
                        shift = response.body();
                        shiftBlocker.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Shift> call, @NonNull Throwable t) {
                Log.e("SHIFT", "NO CURRENT SHIFT");
            }
        });
        ServiceGenerator.createService(APIClient.class, token, getActivity()).getGroups().enqueue(new Callback<GroupRequest>() {
            @Override
            public void onResponse(@NonNull Call<GroupRequest> call, @NonNull Response<GroupRequest> response) {
                if (response.isSuccessful()) {
                    responseCode = response.code();
                    if (responseCode == 200) {
                        GenerateRadioGroups(response.body());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<GroupRequest> call, @NonNull Throwable t) {
                Toast.makeText(getActivity(), "Error getting groups", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * public void scanCode() {
     * Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
     * File photo = new File(Environment.getExternalStorageDirectory(), "pic.jpg");
     * intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
     * Uri imageUri = Uri.fromFile(photo);
     * startActivityForResult(intent, 1);
     * }
     */


    public void addProductToCart() {
        cartAdapter.notifyDataSetChanged();
        calculateTotal();
    }


    public void clearCart() {
        MyAdapter.selectedProducts.clear();
        cartAdapter.notifyDataSetChanged();
        calculateTotal();
    }

    public void calculateTotal() {
        int i = 0;
        total = 0.0;
        MyAdapter.client_type = client.getType();
        Adapter.client_type = client.getType();
        cartAdapter.notifyDataSetChanged();
        adapter.notifyDataSetChanged();

        while (i < MyAdapter.selectedProducts.size()) {
            double price = MyAdapter.selectedProducts.get(i).getPrice();
            if (client.getType() == 1) {
                price = MyAdapter.selectedProducts.get(i).getPricesrd();
            } else if (client.getType() == 2) {
                price = MyAdapter.selectedProducts.get(i).getPriceopt();
            }
            MyAdapter.selectedProducts.get(i).setTotal(price * MyAdapter.selectedProducts.get(i).getCount());
            total = total + price * MyAdapter.selectedProducts.get(i).getCount();
            i++;
        }
        tv_subtotal.setText(NumberFormat.getNumberInstance(Locale.getDefault()).format(total));
        tv_total.setText(NumberFormat.getNumberInstance(Locale.getDefault()).format(total * (1 - Double.valueOf(discount) / 100.0)));
        tv_sum.setText(tv_total.getText());
    }

    public void addUser(View view) {
        dialog = AddClientDialog.newInstance(token, 0);
        dialog.show(this.getFragmentManager(), "Adding_user");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuInflater mInflater = getActivity().getMenuInflater();
        mInflater.inflate(R.menu.main, menu);
        final MenuItem searchItem = menu.findItem(R.id.search);
        //TODO: DEPRECATED
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void openPaymentDialog() {
        final DialogPaymentBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.dialog_payment, null, false);
        payment.setAmount(total * (1.0 - discount / 100.0));
        payment.setToPay(total * (1.0 - discount / 100.0));
        binding.setPayment(payment);
        View subView = binding.getRoot();

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Платеж");
        builder.setMessage("Добавить платеж");
        builder.setView(subView);
        Button btn = subView.findViewById(R.id.payment_done_btn);
        final View addPaymentHolder = subView.findViewById(R.id.add_payment_holder);
        final View closePaymentHolder = subView.findViewById(R.id.close_payment_holder);
        final RadioButton cashRadio = subView.findViewById(R.id.radio_cash);
        final RadioButton cardRadio = subView.findViewById(R.id.radio_card);
        final RadioButton loadRadio = subView.findViewById(R.id.radio_loan);
        final EditText amountET = subView.findViewById(R.id.amount);
        amountET.selectAll();
        final Button closeBtn = subView.findViewById(R.id.payment_close_btn);

        btn.setOnClickListener(v -> {
            double amount = 0;
            String txt_value = amountET.getText().toString();
            if (!txt_value.isEmpty()) {
                try {
                    amount = Double.parseDouble(txt_value);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }

            if (cashRadio.isChecked()) {
                payment.setCash(payment.getCash() + amount);
            } else if (cardRadio.isChecked()) {
                payment.setCard(payment.getCard() + amount);
            } else if (loadRadio.isChecked()) {
                payment.setLoan(payment.getLoan() + amount);
            }
            binding.executePendingBindings();
            binding.setPayment(payment);
            if (payment.getCash() + payment.getCard() + payment.getLoan() >= total * (1.0 - discount / 100.0)) {
                addPaymentHolder.setVisibility(View.GONE);
                closePaymentHolder.setVisibility(View.VISIBLE);
                SendSale("+", closeBtn);
            }
        });


        final AlertDialog dialog = builder.show();
        closeBtn.setOnClickListener(v -> {
            dialog.dismiss();
            discount = 0;
            tv_discount.setText(discount + "");
            setClient(clients.get(0));
            clearCartButton.performClick();
        });
    }

    private void openClientChooseDialog() {
        View v = getActivity().getLayoutInflater()
                .inflate(R.layout.dialog_client, null);

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(String.format("Выберите клиента"));
        builder.setView(v);

        ListView listView = v.findViewById(R.id.client_list);


        final AlertDialog dialog = builder.show();

        ClientsListAdapter clAdapter = new ClientsListAdapter(getActivity(), this, dialog, clients);
        listView.setAdapter(clAdapter);
        EditText clientSearch = v.findViewById(R.id.search_edit);
        clientSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                clAdapter.getFilter().filter(editable);
            }
        });
    }

    public void setClient(Client client) {
        this.client = client;
        tv_client.setText(client.getFullname());
        calculateTotal();
    }

    private void addClientDialog() {


        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.add_client, null);
        final EditText fullname = v.findViewById(R.id.name);
        final EditText phone = v.findViewById(R.id.phone);
        final EditText email = v.findViewById(R.id.email);
        final EditText birthday = v.findViewById(R.id.birthday);
        final Spinner sexSpinner = v.findViewById(R.id.spinner);

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(String.format("Добавить клиента"));
        builder.setView(v);
        Button btn = v.findViewById(R.id.payment_done_btn);
        final Button closeBtn = v.findViewById(R.id.payment_close_btn);

        builder.setPositiveButton("Добавить", (dialog, which) -> {
            final Client client = new Client();
            int type = 0;
            client.setFullname(fullname.getText().toString());
            client.setPhone(phone.getText().toString());
            client.setEmail(email.getText().toString());
            client.setSex(sexSpinner.getSelectedItemPosition() == 0);
            if (((RadioButton)v.findViewById(R.id.type2)).isChecked()) {
                type = 1;
            } else if (((RadioButton)v.findViewById(R.id.type3)).isChecked()) {
                type = 2;
            }
            client.setType(type);

            ServiceGenerator.createService(APIClient.class, token, getActivity()).sendClient(client).enqueue(new Callback<Client>() {
                @Override
                public void onResponse(Call<Client> call, Response<Client> response) {
                    if (response.isSuccessful()) {
                        Client client = response.body();
                        Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                        addClient(client);
                    }
                    dialog.dismiss();
                    calculateTotal();
                }

                @Override
                public void onFailure(Call<Client> call, Throwable t) {
                    Toast.makeText(getActivity(), "Error adding client", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            });
        });

        builder.setNegativeButton("Отменить", (dialog, which) -> dialog.dismiss());

        final AlertDialog dialog = builder.show();
    }

    public void SendSale(String type, final Button btn) {
        Sale sale = new Sale();
        sale.setCart(adapter.selectedProducts);
        uz.ds.coder.RPOS.Product.Payment pm = new uz.ds.coder.RPOS.Product.Payment();
        pm.setAmount(payment.getAmount().doubleValue());
        pm.setCard(payment.getCard().doubleValue());
        pm.setCash(payment.getCash().doubleValue());
        pm.setToPay(payment.getToPay().doubleValue());
        pm.setLoan(payment.getLoan().doubleValue());
        sale.setPayment(pm);
        sale.setType(type);
        sale.setPrint(true);
        sale.setTotal(total * (1 - Double.valueOf(discount) / 100.0));
        sale.setSubtotal(total);
        sale.setDiscount(Double.parseDouble(discount.toString()));
        sale.setClient(client.getId());
        sale.setShift(shift.getId());
        ServiceGenerator.createService(APIClient.class, token, getActivity()).sendSale(sale).enqueue(new Callback<Sale>() {

            @Override
            public void onResponse(Call<Sale> call, Response<Sale> response) {
                if (response.isSuccessful()) {
                    responseCode = response.code();
                    if (responseCode == 200 || responseCode == 201) {
                        discount = 0;
                        Toast.makeText(getActivity(), "Успешно", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Sale> call, Throwable t) {
                Toast.makeText(getActivity(), "Ошибка", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void ChangeDiscountDialog(View view) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View subView = inflater.inflate(R.layout.change_price, null);
        final EditText text = subView.findViewById(R.id.price);
        text.setText(discount.toString());
        text.selectAll();


        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(String.format("Скидка"));
        builder.setMessage(String.format("Сделать скидку"));
        builder.setView(subView);
        builder.setPositiveButton("Сохранить", (dialog, which) -> {
            String txt_value = text.getText().toString();
            if (!txt_value.isEmpty()) {
                try {
                    discount = Double.valueOf(txt_value).intValue();
                    tv_discount.setText(PosFragment.discount.toString());
                    calculateTotal();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }

        });

        builder.setNegativeButton("Отменить", (dialog, which) -> Toast.makeText(getActivity(), "Отмена", Toast.LENGTH_LONG).show());


        final AlertDialog dialog = builder.show();
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        adapter.getFilter().filter(s);
        return true;
    }
}
