package uz.ds.coder.RPOS.Dialog;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import uz.ds.coder.RPOS.R;

/**
 * Created by Coder on 11-Mar-18.
 */

public class PriceChangeDialog extends DialogFragment implements View.OnClickListener {

    private EditText mPrice;
    private Double price;
    private int responseCode = 0;

    public static PriceChangeDialog newInstance(Double price) {
        PriceChangeDialog f = new PriceChangeDialog();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putDouble("price", price);
        f.setArguments(args);

        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_client,container);
        price = getArguments().getDouble("price");
        v.findViewById(R.id.close).setOnClickListener(this);
        v.findViewById(R.id.add).setOnClickListener(this);
        mPrice = v.findViewById(R.id.price);

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.close:
                this.dismiss();
                break;
            case R.id.add:
                Toast.makeText(getActivity(), "Сохранено", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }
}