package uz.ds.coder.RPOS.Activity;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uz.ds.coder.RPOS.Adapter.MyAdapter;
import uz.ds.coder.RPOS.Dialog.AddClientDialog;
import uz.ds.coder.RPOS.Dialog.LogoutRequestDialog;
import uz.ds.coder.RPOS.Fragment.IncomeFragment;
import uz.ds.coder.RPOS.Fragment.PosFragment;
import uz.ds.coder.RPOS.Fragment.SalesFragment;
import uz.ds.coder.RPOS.Product.Client;
import uz.ds.coder.RPOS.Product.Product;
import uz.ds.coder.RPOS.Product.Shift;
import uz.ds.coder.RPOS.R;
import uz.ds.coder.RPOS.Utils.APIClient;
import uz.ds.coder.RPOS.Utils.RequestCodes;
import uz.ds.coder.RPOS.Utils.ServiceGenerator;
import uz.ds.coder.RPOS.models.Payment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private int responseCode = 0;
    private RecyclerView recyclerView;
    private RecyclerView cartRecyclerView;


    private SharedPreferences pref;
    public String token = "";
    ImageButton adduser;
    String title;
    private Spinner clientSpinner;
    public static List<Product> cart;
    public static Double total = 0.0;
    public static Integer discount = 0;
    public static TextView tv_total;
    public static TextView tv_discount;
    public static TextView tv_subtotal;
    public static TextView tv_sum;
    private ImageButton clearCartButton;
    public static EditText product_search;
    private Client client = null;
    AddClientDialog dialog;
    private View pay;
    public static Shift shift;
    private RadioGroup rg;


    private MyAdapter adapter;
    List<Product> products;
    List<Client> clients;
    private Payment payment;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        title = getResources().getString(R.string.app_name);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(title);

        pref = PreferenceManager.getDefaultSharedPreferences(this);
        token = pref.getString("TOKEN", "");

        if (token == "") {
            Intent logIntent = new Intent(this, LoginActivity.class);
            startActivityForResult(logIntent, RequestCodes.REQUEST_LOGIN);
        } else {
            loadPosFragment();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


    }

    private void loadPosFragment() {
        PosFragment fragment = new PosFragment();
        getFragmentManager().beginTransaction().replace(R.id.content, fragment, "posFragment").commit();
    }

    public void closeShift() {
        PosFragment fragment = (PosFragment) getFragmentManager().findFragmentByTag("posFragment");
        if (fragment.shift.getId() == null) {
            Toast.makeText(this, getString(R.string.shift_closed), Toast.LENGTH_SHORT).show();
        } else {
            ServiceGenerator.createService(APIClient.class, token, this).closeShift(fragment.shift)
                    .enqueue(new Callback<Shift>() {

                        @Override
                        public void onResponse(@NonNull Call<Shift> call, @NonNull Response<Shift> response) {
                            Log.e("RESPONSE", response.message());
                            if (response.isSuccessful()) {
                                responseCode = response.code();
                                if (responseCode == 200) {
                                    shift = response.body();
                                    fragment.shiftBlocker.setVisibility(View.VISIBLE);
                                }
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<Shift> call, @NonNull Throwable t) {
                            Toast.makeText(MainActivity.this, "Ошибка", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RequestCodes.LOGIN_SUCCESSFULL) {
            Toast.makeText(this, "not logged", Toast.LENGTH_SHORT).show();
        } else {
//            LoadDataFromServer();
            loadPosFragment();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar cart_item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
            //Toast.makeText(this,"clicked Settings",Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view cart_item clicks here.
        int id = item.getItemId();

        Fragment fragment = null;
        String tag = "";
        if (id == R.id.income) {
            title = "Income";
            fragment = new IncomeFragment();
            tag = "incomeFragment";
        } else if (id == R.id.pos) {
            title = "Sales";
            fragment = new PosFragment();
            tag = "posFragment";
        } else if (id == R.id.close_shift) {
            closeShift();
        } else if (id == R.id.sales) {
            title = getString(R.string.sales_title);
            tag = "salesFragment";
            fragment = new SalesFragment();
        }



        if (fragment != null) {
            getFragmentManager().beginTransaction().replace(R.id.content, fragment, tag).commit();
            getSupportActionBar().setTitle(title);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

//    public void addClient(Client client) {
//        clients.add(client);
//        clientSpinner.setSelection(clients.size() - 1);
//    }
//
//    public void deleteProductFromCart(Product product) {
//        MyAdapter.selectedProducts.remove(product);
//        cartAdapter.notifyDataSetChanged();
//        calculateTotal();
//    }
//
//    public void refreshCart() {
//        cartAdapter.notifyDataSetChanged();
//        calculateTotal();
//    }
//
//    public void addProductToCart() {
//        cartAdapter.notifyDataSetChanged();
//        calculateTotal();
//    }


    public void logoutclick(View view) {
        findViewById(R.id.logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogoutRequestDialog dlg = new LogoutRequestDialog();
                dlg.show(getFragmentManager(), "logoutRequest");
            }
        });
    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
    }
}