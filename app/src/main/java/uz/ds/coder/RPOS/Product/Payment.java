
package uz.ds.coder.RPOS.Product;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import uz.ds.coder.RPOS.BR;
import uz.ds.coder.RPOS.Utils.TextFormatters;

public class Payment extends BaseObservable {

    @SerializedName("card")
    @Expose
    private Double card;
    @SerializedName("cash")
    @Expose
    private Double cash;
    @SerializedName("loan")
    @Expose
    private Double loan;

    @Bindable
    public Double getLoanDouble() {
        return loan;
    }

    @Bindable
    public String getLoan() {
        return TextFormatters.priceFormatter().format(loan);
    }

    public void setLoan(Double loan) {
        this.loan = loan;
        notifyPropertyChanged(BR.cash);
    }

    @SerializedName("done")
    @Expose
    private Boolean done;
    @SerializedName("amount")
    @Expose
    private Double amount;
    @SerializedName("change")
    @Expose
    private Double change;
    @SerializedName("toPay")
    @Expose
    private Double toPay;
    @SerializedName("type")
    @Expose
    private Integer type;

    public Double getToPay() {
        return toPay;
    }

    public void setToPay(Double toPay) {
        this.toPay = toPay;
    }


    public Double getCardDouble() {
        return card;
    }

    @Bindable
    public String getCard() {
        return TextFormatters.priceFormatter().format(card);
    }

    public void setCard(Double card) {
        this.card = card;
        notifyPropertyChanged(BR.card);
    }

    public Double getCashDouble() {
        return cash;
    }
    @Bindable
    public String getCash() {
        return TextFormatters.priceFormatter().format(cash);
    }

    public void setCash(Double cash) {
        this.cash = cash;
        notifyPropertyChanged(BR.cash);
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getChange() {
        return change;
    }

    public void setChange(Double change) {
        this.change = change;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer isCash) {
        this.type = type;
    }

}
