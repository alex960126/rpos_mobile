package uz.ds.coder.RPOS.Utils;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by bakha on 13.01.2018.
 */

public interface UserService {
    @GET("/api/me/")
    Call<UserCredentials> me();


}