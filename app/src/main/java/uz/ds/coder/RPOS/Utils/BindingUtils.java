package uz.ds.coder.RPOS.Utils;

import android.databinding.BindingAdapter;
import android.databinding.InverseBindingAdapter;
import android.widget.TextView;

/**
 * Created by bakha on 17.03.2018.
 */

public class BindingUtils {

    @BindingAdapter("android:text")
    public static void setFloat(TextView view, double value) {
        if (Double.isNaN(value)) view.setText("");
        else view.setText(Double.valueOf(value).toString());
    }

    @InverseBindingAdapter(attribute = "android:text")
    public static double getFloat(TextView view) {
        String num = view.getText().toString();
        if(num.isEmpty()) return 0.0F;
        try {
            return Double.parseDouble(num);
        } catch (NumberFormatException e) {
            return 0.0F;
        }
    }
}