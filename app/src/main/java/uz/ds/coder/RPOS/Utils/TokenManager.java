package uz.ds.coder.RPOS.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TokenManager {

    private SharedPreferences prefs;

    TokenManager(Context context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setToken(String token) {
        prefs.edit().putString(Constants.TOKEN, token).apply();
        Log.d("TOKEN","Saved new token: " + token);
    }

    public String getToken() {
        return prefs.getString(Constants.TOKEN, "");
    }

    String refreshToken() {
        TokenRefresh refresh = ServiceGenerator.createService(TokenRefresh.class);
        try {

            JSONObject paramObject = new JSONObject();
            paramObject.put("token", getToken());
            String sToken = getToken();
            clearToken();
            final Token[] token = new Token[1];
            refresh.getToken(sToken).enqueue(new Callback<Token>() {
                @Override
                public void onResponse(Call<Token> call, Response<Token> response) {
                    if (response.code() == 200) {
                        token[0] = response.body();
                        assert token[0] != null;
                        Log.e("RETROFIT2", token[0].getToken() + " s");
                        setToken(token[0].getToken());
                        Log.d("TOKEN", "NEW TOKEN ADDED");
                    } else {
                        Log.e("RETROFIT2", response.message());
                    }
                }

                @Override
                public void onFailure(Call<Token> call, Throwable t) {

                }
            });
            if (token[0]!=null)
                return token[0].getToken();
            else return null;
        } catch (JSONException ex) {
            ex.printStackTrace();
            return "";
        }
    }

    public boolean hasToken() {
        return prefs.contains(Constants.TOKEN);
    }

    private void clearToken() {
        prefs.edit().remove(Constants.TOKEN).apply();
    }
}
