package uz.ds.coder.RPOS.Utils

import com.google.gson.annotations.SerializedName

/**
 * Created by bakha on 13.01.2018.
 */
public data class Token(
    @field:SerializedName("token")
    var token: String? = null,

    @field:SerializedName("non_field_errors")
    var non_field_errors: String? = null


)