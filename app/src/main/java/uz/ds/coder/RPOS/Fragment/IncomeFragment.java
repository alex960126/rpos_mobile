package uz.ds.coder.RPOS.Fragment;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uz.ds.coder.RPOS.Activity.MainActivity;
import uz.ds.coder.RPOS.Adapter.Adapter;
import uz.ds.coder.RPOS.Adapter.AgentSpinnerAdapter;
import uz.ds.coder.RPOS.Adapter.MyAdapter;
import uz.ds.coder.RPOS.Dialog.AddClientDialog;
import uz.ds.coder.RPOS.Product.Agent;
import uz.ds.coder.RPOS.Product.Group;
import uz.ds.coder.RPOS.Product.GroupRequest;
import uz.ds.coder.RPOS.Product.Income;
import uz.ds.coder.RPOS.Product.Product;
import uz.ds.coder.RPOS.Product.Shift;
import uz.ds.coder.RPOS.R;
import uz.ds.coder.RPOS.Utils.APIClient;
import uz.ds.coder.RPOS.Utils.ServiceGenerator;
import uz.ds.coder.RPOS.databinding.DialogPaymentIncomeBinding;
import uz.ds.coder.RPOS.models.Payment;

/**
 * A simple {@link Fragment} subclass.
 */
public class IncomeFragment extends android.app.Fragment {


    private int responseCode = 0;
    private RecyclerView recyclerView;
    private RecyclerView cartRecyclerView;

    private MyAdapter adapter;
    private Adapter cartAdapter;
    private SharedPreferences pref;
    public String token = "";
    ImageButton adduser;
    String title;
    private Spinner clientSpinner;
    public static List<Product> cart;
    public static Double total = 0.0;
    public static Integer discount = 0;
    public static TextView tv_total;
    public static TextView tv_discount;
    public static TextView tv_subtotal;
    public static TextView tv_sum;
    private ImageButton clearCartButton;
    public static EditText product_search;
    private Agent client = null;
    AddClientDialog dialog;
    private View pay;
    private Shift shift;
    private RadioGroup rg;



    List<Product> products;
    List<Agent> clients;
    private Payment payment;

    public IncomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_income, container, false);

        payment = new Payment();

        pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        token = pref.getString("TOKEN", "");
        products = new ArrayList<>();
        recyclerView = (RecyclerView) v.findViewById(R.id.list);
        cartRecyclerView = (RecyclerView) v.findViewById(R.id.cart_recycler);
        clearCartButton = v.findViewById(R.id.clearCart);
        product_search = v.findViewById(R.id.product_search);
        dialog = AddClientDialog.newInstance(token, 1);

        pay = v.findViewById(R.id.pay);
        pay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                openPaymentDialog();
            }
        });
        shift = new Shift();
        rg = v.findViewById(R.id.products_group);

        v.findViewById(R.id.discount_holder).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        v.findViewById(R.id.add_user).setOnClickListener(v12 -> addClientDialog());

        v.findViewById(R.id.discount_change).setOnClickListener(v1 -> ChangeDiscountDialog(v1));




        clientSpinner = v.findViewById(R.id.buyer);
        tv_total = (TextView) v.findViewById(R.id.tv_total);
        tv_discount = v.findViewById(R.id.tv_discount);
        tv_discount.setText(discount.toString());
        tv_subtotal = v.findViewById(R.id.tv_subtotal);
        tv_sum = v.findViewById(R.id.sum);

        LoadDataFromServer();

        return v;
    }


    public void convertContainer(Product product) {
        Product product1 = MyAdapter.selectedProducts.get(MyAdapter.selectedProducts.indexOf(product));
        if (product1.getIncontainer() > 0) {
            product1.setCount(product1.getCount() * product1.getIncontainer());
        }
        cartAdapter.notifyDataSetChanged();
        calculateTotal();
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void openPaymentDialog(){
        final DialogPaymentIncomeBinding binding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.dialog_payment_income, null, false);
        payment.setAmount(total * (1 - discount / 100));
        payment.setToPay(total * (1 - discount / 100));
        binding.setPayment(payment);
        View subView = binding.getRoot();

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(String.format("Платеж"));
        builder.setMessage(String.format("Добавить платеж"));
        builder.setView(subView);
        Button btn = subView.findViewById(R.id.payment_done_btn);
        final View addPaymentHolder = subView.findViewById(R.id.add_payment_holder);
        final View closePaymentHolder = subView.findViewById(R.id.close_payment_holder);
        final RadioButton cashRadio = subView.findViewById(R.id.radio_cash);
        final EditText amountET = subView.findViewById(R.id.amount);
        amountET.selectAll();
        final Button closeBtn = subView.findViewById(R.id.payment_close_btn);

        btn.setOnClickListener(v -> {
            double amount = 0;
            String txt_value = amountET.getText().toString();
            if (!txt_value.isEmpty()) {
                try {
                    amount = Double.parseDouble(txt_value);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }

            if (cashRadio.isChecked()) {
                payment.setCash(payment.getCash() + amount);
            } else {
                payment.setCard(payment.getCard() + amount);
            }
            binding.executePendingBindings();
            binding.setPayment(payment);
            if (payment.getCash() + payment.getCard() >= total) {
                addPaymentHolder.setVisibility(View.GONE);
                closePaymentHolder.setVisibility(View.VISIBLE);
                SendSale(closeBtn);
            }
        });

        final AlertDialog dialog = builder.show();
        closeBtn.setOnClickListener(v -> {
            dialog.dismiss();
            clearCartButton.performClick();
        });

    }

    public void SendSale(final Button btn) {
        Income income = new Income();
        income.setProducts(adapter.selectedProducts);
        uz.ds.coder.RPOS.Product.Payment pm = new uz.ds.coder.RPOS.Product.Payment();
        pm.setAmount(payment.getAmount().doubleValue());
        pm.setCard(payment.getCard().doubleValue());
        pm.setCash(payment.getCash().doubleValue());
        pm.setToPay(payment.getToPay().doubleValue());
        income.setPayment(pm);
        income.setTotal(total * (1 - Double.valueOf(discount) / 100.0));
        Gson gson = new Gson();
        Log.e("RETROFIT2", gson.toJson(income));
        ServiceGenerator.createService(APIClient.class, token, getActivity()).sendIncome(income).enqueue(new Callback<Income>(){

            @Override
            public void onResponse(Call<Income> call, Response<Income> response) {
                if (response.isSuccessful()) {
                    responseCode = response.code();
                    if (responseCode == 200) {
                        Toast.makeText(getActivity(), "Успешно", Toast.LENGTH_SHORT).show();
                        btn.setEnabled(true);
                    }
                }
            }

            @Override
            public void onFailure(Call<Income> call, Throwable t) {
                Log.e("RETROFIT2 ERROR",t.getMessage());
                Toast.makeText(getActivity(), "Ошибка", Toast.LENGTH_SHORT).show();
                btn.setEnabled(true);
            }
        });


    }

    public void GenerateRadioGroups(GroupRequest groupRequest) {
        int i = 1;
        RadioButton radio = new RadioButton(getActivity());
        radio.setId(0);
        radio.setText("Все");
        radio.setChecked(true);
        radio.setPadding(10, 10, 10, 10);
        radio.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
        radio.setButtonDrawable(android.R.color.transparent);
        radio.setOnClickListener(v -> adapter.getFilter().filter(""));
        rg.addView(radio);
        for(Group gr : groupRequest.getResults()) {

            radio = new RadioButton(getActivity());
            final String title = gr.getTitle();
            radio.setText(gr.getTitle());
            radio.setId(i++);
            radio.setBackground(getResources().getDrawable(R.drawable.rbtn_selector));
            radio.setButtonDrawable(android.R.color.transparent);
            radio.setPadding(10, 10, 10, 10);
            radio.setOnClickListener(v -> adapter.getFilter().filter(title));
            rg.addView(radio);
        }
    }


    public void LoadDataFromServer() {
        ServiceGenerator.createService(APIClient.class,token, getActivity()).getlistProducts().
                enqueue(new Callback<List<Product>>() {
                    @Override
                    public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                        if (response.isSuccessful()) {
                            responseCode = response.code();
                            if (responseCode==200) {
                                products.addAll(response.body());
                                GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 4);
                                recyclerView.setLayoutManager(layoutManager);
                                adapter = new MyAdapter(products, getActivity(), IncomeFragment.this, 1);
                                recyclerView.setAdapter(adapter);
                                recyclerView.setHasFixedSize(true);
                                recyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
                                recyclerView.getRecycledViewPool().setMaxRecycledViews(0, 0);

                                cartRecyclerView.setHasFixedSize(true);
                                cartRecyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
                                cartRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL,false));
                                cartRecyclerView.getRecycledViewPool().setMaxRecycledViews(0, 0);

                                cartAdapter = new Adapter(getActivity(), MyAdapter.selectedProducts, IncomeFragment.this, 1, 0);
                                cartRecyclerView.setAdapter(cartAdapter);

                                product_search.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {
                                        adapter.getFilter().filter(s.toString());
                                    }
                                });


                                clearCartButton.setOnClickListener(v -> {
                                    MyAdapter.selectedProducts.clear();
                                    payment = new Payment();
                                    cartAdapter.notifyDataSetChanged();
                                    calculateTotal();
                                });

                                calculateTotal();


                                cart = new ArrayList<>();
                            } else {
                                Toast.makeText(getActivity(), "Данных нет", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(),"response.code() != 200",Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Product>> call, Throwable t) {
                        Toast.makeText(getActivity(), t.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                });
        ServiceGenerator.createService(APIClient.class,token, getActivity()).getlistAgents().enqueue(new Callback<List<Agent>>() {
            @Override
            public void onResponse(Call<List<Agent>> call, Response<List<Agent>> response) {
                if (response.isSuccessful()) {
                    responseCode = response.code();
                    if (responseCode == 200) {
                        clients = new ArrayList<>();
                        clients.addAll(response.body());

                        if (client == null) {
                            client = clients.get(0);
                        }
                        AgentSpinnerAdapter adapter = new AgentSpinnerAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, android.R.id.text1, clients);
                        clientSpinner.setAdapter(adapter);
                        clientSpinner.setSelection(0);
                        clientSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                client = clients.get(position);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                client = clients.get(0);
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Agent>> call, Throwable t) {
                Toast.makeText(getActivity(), "Error getting clients", Toast.LENGTH_SHORT).show();
            }
        });
        ServiceGenerator.createService(APIClient.class,token, getActivity()).getCurrentShift().enqueue(new Callback<Shift>() {
            @Override
            public void onResponse(Call<Shift> call, Response<Shift> response) {
                if (response.isSuccessful()) {
                    responseCode = response.code();
                    if (responseCode == 200) {
                        shift = response.body();
                    }
                }
            }

            @Override
            public void onFailure(Call<Shift> call, Throwable t) {
                Toast.makeText(getActivity(), "Error getting clients", Toast.LENGTH_SHORT).show();
            }
        });
        ServiceGenerator.createService(APIClient.class,token, getActivity()).getGroups().enqueue(new Callback<GroupRequest>() {
            @Override
            public void onResponse(Call<GroupRequest> call, Response<GroupRequest> response) {
                if (response.isSuccessful()) {
                    responseCode = response.code();
                    if (responseCode == 200) {
                        GenerateRadioGroups(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<GroupRequest> call, Throwable t) {
                Toast.makeText(getActivity(), "Error getting clients", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void addClient(Agent client) {
        clients.add(client);
        clientSpinner.setSelection(clients.size() - 1);
    }



    public void deleteProductFromCart(Product product) {
        MyAdapter.selectedProducts.remove(product);
        cartAdapter.notifyDataSetChanged();
        calculateTotal();
    }

    public void clearCart() {
        MyAdapter.selectedProducts.clear();
        cartAdapter.notifyDataSetChanged();
        calculateTotal();
    }

    public void refreshCart() {
        cartAdapter.notifyDataSetChanged();
        calculateTotal();
    }



    public static void calculateTotal(){
        int i=0;
        total=0.0;
        while(i<MyAdapter.selectedProducts.size()){
            total=total + Double.valueOf(MyAdapter.selectedProducts.get(i).getPrice()) * Double.valueOf(MyAdapter.selectedProducts.get(i).getCount());
            i++;
        }
        tv_subtotal.setText(NumberFormat.getNumberInstance(Locale.getDefault()).format(total));
        tv_total.setText(NumberFormat.getNumberInstance(Locale.getDefault()).format(total * (1 - Double.valueOf(discount) / 100.0)));
        tv_sum.setText(tv_total.getText());
    }

    public void addUser(View view) {
        dialog = AddClientDialog.newInstance(token, 0);
        dialog.show(getActivity().getFragmentManager(),"Adding_user");
    }

    private void addClientDialog(){


        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.add_agent, null);
        final EditText fullname = v.findViewById(R.id.name);
        final EditText phone = v.findViewById(R.id.phone);
        final EditText email = v.findViewById(R.id.email);
        final EditText birthday = v.findViewById(R.id.birthday);
        final Spinner sexSpinner = v.findViewById(R.id.spinner);

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(String.format("Добавить клиента"));
        builder.setView(v);
        Button btn = v.findViewById(R.id.payment_done_btn);
        final Button closeBtn = v.findViewById(R.id.payment_close_btn);

        builder.setPositiveButton("Добавить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int which) {
                final Agent client = new Agent();
                client.setFullname(fullname.getText().toString());
                client.setPhone(phone.getText().toString());
                client.setCompany(email.getText().toString());

                ServiceGenerator.createService(APIClient.class, token, getActivity()).sendAgent(client).enqueue(new Callback<Agent>() {
                    @Override
                    public void onResponse(Call<Agent> call, Response<Agent> response) {
                        if (response.isSuccessful()) {
                            if (response.code() == 201) {
                                Agent client = response.body();
                                Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                                addClient(client);
                            } else {
                                Toast.makeText(getActivity(), "Ошибка", Toast.LENGTH_SHORT).show();
                            }

                        }
                        dialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<Agent> call, Throwable t) {
                        Toast.makeText(getActivity(), "Error adding client", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
            }
        });

        builder.setNegativeButton("Отменить", (dialog, which) -> dialog.dismiss());

        final AlertDialog dialog = builder.show();
    }


    public void addProductToCart() {
        cartAdapter.notifyDataSetChanged();
        calculateTotal();
    }

    public void ChangeDiscountDialog(View view) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View subView = inflater.inflate(R.layout.change_price, null);
        final EditText text = subView.findViewById(R.id.price);
        text.setText(discount.toString());
        text.selectAll();


        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(String.format("Скидка"));
        builder.setMessage(String.format("Сделать скидку"));
        builder.setView(subView);
        builder.setPositiveButton("Сохранить", (dialog, which) -> {
            String txt_value = text.getText().toString();
            if (!txt_value.isEmpty()) {
                try {
                    discount = Double.valueOf(txt_value).intValue();
                    tv_discount.setText(MainActivity.discount.toString());
                    calculateTotal();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }

        });

        builder.setNegativeButton("Отменить", (dialog, which) -> Toast.makeText(getActivity(), "Отмена", Toast.LENGTH_LONG).show());


        final AlertDialog dialog = builder.show();
    }

    /**
     * public void scanCode() {
     Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
     File photo = new File(Environment.getExternalStorageDirectory(), "pic.jpg");
     intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
     Uri imageUri = Uri.fromFile(photo);
     startActivityForResult(intent, 1);
     }
     */
}
