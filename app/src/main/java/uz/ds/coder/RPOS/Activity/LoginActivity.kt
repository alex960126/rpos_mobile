package uz.ds.coder.RPOS.Activity

import android.content.Intent
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONException
import org.json.JSONObject
import uz.ds.coder.RPOS.R
import uz.ds.coder.RPOS.Utils.*
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.Reader

class LoginActivity : AppCompatActivity() {
    private var mAuthTask: UserLoginTask? = null

    private lateinit var pref : SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        pref = PreferenceManager.getDefaultSharedPreferences(this)

        val token = pref.getString("TOKEN", "")

        if (token != "") {
            setResult(RequestCodes.LOGIN_SUCCESSFULL)
            finish()
        }

        password.setOnEditorActionListener(TextView.OnEditorActionListener { _, id, _ ->
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptLogin()
                return@OnEditorActionListener true
            }
            false
        })

        login_in_button.setOnClickListener { attemptLogin() }
    }

    fun signInAction() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    private fun attemptLogin() {
        if (mAuthTask != null) {
            return
        }

        // Reset errors.
        login.error = null
        password.error = null

        // Store values at the time of the login attempt.
        val loginStr = login.text.toString()
        val passwordStr = password.text.toString()

        var cancel = false
        var focusView: View? = null

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(passwordStr) && !isPasswordValid(passwordStr)) {
            password.error = getString(R.string.error_invalid_password)
            focusView = password
            cancel = true
        }

        // Check for a valid login.
        if (TextUtils.isEmpty(loginStr)) {
            login.error = getString(R.string.error_field_required)
            focusView = login
            cancel = true
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView?.requestFocus()
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true)
            val task = UserLoginTask(loginStr, passwordStr)
            task.execute()

        }
    }


    override fun onDestroy() {
        super.onDestroy()
        setResult(RequestCodes.LOGIN_UNSUCCESSFULL)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RequestCodes.REQUEST_SIGN_IN) {
            if (resultCode == RequestCodes.SIGNIN_SUCCESSFULL) {
                val usernameExtra = data?.getStringExtra("username")
                val passwordExtra = data?.getStringExtra("password")
                login.setText(usernameExtra)
                password.setText(passwordExtra)
                login_in_button.performClick()
            }
        }
    }

    private fun isPasswordValid(password: String): Boolean {
        return password.length > 4
    }

    private fun showProgress(show: Boolean) {
        login_progress.visibility = if (show) View.VISIBLE else View.GONE
        login_form.visibility = if (show) View.GONE else View.VISIBLE
    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    inner class UserLoginTask internal constructor(private val mLogin: String, private val mPassword: String) : AsyncTask<Void, Void, Boolean>() {

        private var responseText : String? = null

        override fun doInBackground(vararg params: Void): Boolean? {
            // TODO: attempt authentication against a network service.

            var tokenService = ServiceGenerator.createService(LoginInterface::class.java)
            try {
                val paramObject = JSONObject()
                paramObject.put("username", mLogin)
                paramObject.put("password", mPassword)
                val call = ServiceGenerator.createService(LoginInterface::class.java).getToken(paramObject.toString()).execute()
                val token = call.body()
                val error = call.errorBody()
                if (token != null) {
                    if (token.token != null) {
                        responseText = token.token
                        pref.edit().putString(
                                Constants.TOKEN,
                                token.token.toString()
                        ).apply()
                        val call = ServiceGenerator.createService(UserService::class.java, token.token, applicationContext).me().execute()
                        val user = call.body()
                        if (user == null) {
                            val sb = StringBuilder()
                            try {
                                val reader = BufferedReader(InputStreamReader(call.errorBody()!!.byteStream()) as Reader?)
                                var line: String?
                                try {
                                    line = reader.readLine()
                                    while (line != null) {
                                        sb.append(line)
                                        line = reader.readLine()
                                    }
                                } catch (ex:IOException) {
                                    ex.printStackTrace()
                                }
                            } catch (ex: IOException) {
                                ex.printStackTrace()
                            }
                            val error = sb.toString()
                            Log.e("TAG", error)
                            return false
                        }
                        val gson = Gson()
                         //GsonBuilder().setPrettyPrinting().create()

                        pref.edit().putString("USER", gson.toJson(user)).putInt("USERID", user!!.id!!).apply()
                        return true
                    }
                } else {
                    val sb = StringBuilder()
                    try {
                        var reader = BufferedReader(InputStreamReader(error!!.byteStream()))
                        var line : String? = null
                        try {
                            line = reader.readLine()
                            while (line != null) {
                                sb.append(line)
                                line = reader.readLine()
                            }
                        } catch (ex:IOException) {
                            ex.printStackTrace()
                        }
                    } catch (ex: IOException) {
                        ex.printStackTrace()
                    }
                    val error = sb.toString()
                    return false
                }


            } catch (ex : JSONException) {
                ex.printStackTrace()
            } catch (ex : Exception) {
                ex.printStackTrace()
            }


            return false
        }

        override fun onPostExecute(success: Boolean?) {
            mAuthTask = null
            showProgress(false)
            response.text = responseText

            if (success!!) {
                setResult(RequestCodes.LOGIN_SUCCESSFULL)
                finish()
            } else {
                password.error = getString(R.string.error_incorrect_password)
                password.requestFocus()
            }
        }

        override fun onCancelled() {
            mAuthTask = null
            showProgress(false)
        }
    }

}
