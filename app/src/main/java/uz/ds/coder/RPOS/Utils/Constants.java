package uz.ds.coder.RPOS.Utils;

/**
 * Created by bakha on 14.01.2018.
 */

public class Constants {
    public static String SERVER_URL = "http://starteks.uz/";
    public static String API_CLIENTS = SERVER_URL + "api/clients/";
    public static String API_PRODUCTS = SERVER_URL + "api/products/";
    public static String API_SHOPS = SERVER_URL + "api/shops/";
    public static String TOKEN = "TOKEN";
    public static String USER = "USER";
}
