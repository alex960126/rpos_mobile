package uz.ds.coder.RPOS.Adapter;

/**
 * Created by Dell on 16.03.2018.
 */

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import uz.ds.coder.RPOS.Product.Agent;

public class AgentSpinnerAdapter extends ArrayAdapter<Agent> {

    LayoutInflater flater;

    public AgentSpinnerAdapter(Activity context, int resouceId, int textviewId, List<Agent> list){

        super(context,resouceId,textviewId, list);
        flater = context.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Agent rowItem = getItem(position);

        View rowview = flater.inflate(android.R.layout.simple_spinner_item,null,true);

        TextView txtTitle = (TextView) rowview.findViewById(android.R.id.text1);
        txtTitle.setText(rowItem.getFullname());

        return rowview;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = flater.inflate(android.R.layout.simple_spinner_dropdown_item,parent, false);
        }
        Agent rowItem = getItem(position);
        TextView txtTitle = (TextView) convertView.findViewById(android.R.id.text1);
        txtTitle.setText(rowItem.getFullname());
        return convertView;
    }
}