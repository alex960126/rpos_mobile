package uz.ds.coder.RPOS.Utils;

import android.content.Context;
import android.text.TextUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ServiceGenerator {


    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(Constants.SERVER_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();

    public static <S> S createService(Class<S> serviceClass) {
        builder.client(httpClient.build());
        retrofit = builder.build();
        return retrofit.create(serviceClass);
    }



    public static <S> S createService(
            Class<S> serviceClass, final String authToken, Context mContext) {
        if (!TextUtils.isEmpty(authToken)) {
            TokenManager manager = new TokenManager(mContext);
            AuthenticationInterceptor interceptor =
                    new AuthenticationInterceptor(manager);

            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor);

                builder.client(httpClient.build());
                retrofit = builder.build();
            }
        }

        return retrofit.create(serviceClass);
    }

    public static class AuthenticationInterceptor implements Interceptor {

        private TokenManager mTokenManager;

        public AuthenticationInterceptor(TokenManager mTokenManager) {
            this.mTokenManager = mTokenManager;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();

            Request.Builder builder = original.newBuilder()
                    .header("Authorization",  "JWT " + mTokenManager.getToken());

            Request request = builder.build();
            Response response = chain.proceed(request);
            boolean unauthorized = response.code() == 401;
            if (unauthorized) {
                String newToken = mTokenManager.refreshToken();
                request = request.newBuilder()
                        .addHeader("Authorization", "JWT " + newToken)
                        .build();
                return chain.proceed(request);
            }
            return response;
        }
    }
}