package uz.ds.coder.RPOS.Utils

import com.google.gson.annotations.SerializedName

/**
 * Created by bakha on 13.01.2018.
 */
public data class UserCredentials(
    @field:SerializedName("id")
    var id: Int? = null,
    @field:SerializedName("first_name")
    var first_name: String? = null,
    @field:SerializedName("last_name")
    var last_name: String? = null,
    @field:SerializedName("username")
    var username: String? = null,
    @field:SerializedName("email")
    var email: String? = null
)