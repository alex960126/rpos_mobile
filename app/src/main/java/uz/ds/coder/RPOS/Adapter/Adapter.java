package uz.ds.coder.RPOS.Adapter;

import android.app.Fragment;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import uz.ds.coder.RPOS.Fragment.IncomeFragment;
import uz.ds.coder.RPOS.Fragment.PosFragment;
import uz.ds.coder.RPOS.Product.Product;
import uz.ds.coder.RPOS.R;
import uz.ds.coder.RPOS.Utils.TextFormatters;

/**
 * Created by Coder on 14-Mar-18.
 */

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {
    private List<Product> products;
    private Context context;
    private int ftype;
    public static int client_type;
    private Fragment fragment;


    public Adapter(Context context, List<Product> products, Fragment fragment, int ftype, int client_type)  {
        this.products = products;
        this.context = context;
        this.fragment = fragment;
        this.ftype = ftype;
        this.client_type = client_type;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public Adapter.ViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_item, parent, false);
        return new Adapter.ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(Adapter.ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final Product product = products.get(position);
        holder.title.setText(product.getTitle());
        final double price =
                (client_type == 0) ? product.getPrice() :
                        (client_type == 1) ? product.getPricesrd() :
                                product.getPriceopt();

        holder.price.setText(TextFormatters.priceFormatter().format(price));
        holder.amount.setText(String.format("%.2f", product.getCount()));
        Double total = (price * product.getCount());
        holder.total.setText(TextFormatters.priceFormatter().format(total));
        holder.packageButton.setOnClickListener(view -> {
            if (ftype == 0)
                ((PosFragment)fragment).convertContainer(product);
            else
                ((IncomeFragment)fragment).convertContainer(product);
            view.setEnabled(false);
            /* Toast.makeText(context, "CLICKED", Toast.LENGTH_SHORT).show(); */
        });
        holder.deleteButton.setOnClickListener(v -> {
            if (ftype == 0)
                ((PosFragment)fragment).deleteProductFromCart(product);
            else
                ((IncomeFragment)fragment).deleteProductFromCart(product);
        });
        holder.price.setOnClickListener(v -> openDialog(product,0));
        holder.amount.setOnClickListener(view -> openDialog(product, 1));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return products.size();
    }

    // Provide a reference to the views for each data cart_item
    // Complex data items may need more than one view per cart_item, and
    // you provide access to all the views for a data cart_item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data cart_item is just a string in this case
        View mView;
        TextView title;
        TextView price;
        TextView amount;
        TextView total;
        ImageButton deleteButton;
        ImageButton packageButton;
        public ViewHolder(View v) {
            super(v);
            mView = v.findViewById(R.id.content_holder);
            title = v.findViewById(R.id.title);
            amount = v.findViewById(R.id.count);
            price = v.findViewById(R.id.price);
            deleteButton = v.findViewById(R.id.delete);
            packageButton = v.findViewById(R.id.to_container_btn);

            total = v.findViewById(R.id.total);
        }
    }

    String[] dialogTitles = new String[]{"цену", "количество"};

    private void openDialog(final Product product, final int type){
        LayoutInflater inflater = LayoutInflater.from(context);
        View subView = inflater.inflate(R.layout.change_price, null);
        final EditText subEditText = subView.findViewById(R.id.price);
        subEditText.setText((type == 0) ? product.getPrice().toString() : product.getCount().toString());
        subEditText.selectAll();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(String.format("Изменить %s", dialogTitles[type]));
        builder.setMessage(String.format("Введите %s", dialogTitles[type]));
        builder.setView(subView);
        builder.create();

        builder.setPositiveButton("Сохранить", (dialog, which) -> {
            if (type == 0) {
                product.setPrice(Double.valueOf(subEditText.getText().toString()));
            } else {
                product.setCount(Double.valueOf(subEditText.getText().toString()));
            }
            product.setTotal(product.getCount() * product.getPrice() * (1 - product.getDiscount() / 100));
            if (ftype == 0) {
                ((PosFragment)fragment).refreshCart();

            } else {
                ((IncomeFragment)fragment).refreshCart();
            }
        });

        builder.setNegativeButton("Отменить", (dialog, which) -> Toast.makeText(context, "Cancel", Toast.LENGTH_LONG).show());

        builder.show();
    }

}

