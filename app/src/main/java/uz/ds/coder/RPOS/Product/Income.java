
package uz.ds.coder.RPOS.Product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Income {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("products")
    @Expose
    private List<Product> products = null;
    @SerializedName("payment")
    @Expose
    private Payment payment;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("total")
    @Expose
    private Double total;
    @SerializedName("agent")
    @Expose
    private Integer agent;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Integer getAgent() {
        return agent;
    }

    public void setAgent(Integer agent) {
        this.agent = agent;
    }

}
