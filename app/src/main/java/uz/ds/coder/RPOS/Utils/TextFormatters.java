package uz.ds.coder.RPOS.Utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class TextFormatters {
    public static DecimalFormat priceFormatter() {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setGroupingSeparator(' ');
        symbols.setDecimalSeparator(',');

        return new DecimalFormat("$ #,###.00", symbols);
    }
}
