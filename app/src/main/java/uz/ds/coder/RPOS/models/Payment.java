package uz.ds.coder.RPOS.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.PropertyChangeRegistry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by bakha on 17.03.2018.
 */

public class Payment extends BaseObservable{
    private PropertyChangeRegistry registry =
            new PropertyChangeRegistry();

    @SerializedName("card")
    @Expose
    private Double card;
    @SerializedName("cash")
    @Expose
    private Double cash;
    @SerializedName("loan")
    @Expose
    private Double loan;

    public Double getLoan() {
        return loan;
    }

    public void setLoan(Double loan) {
        this.loan = loan;
    }

    @SerializedName("amount")
    @Expose
    private Double amount;
    @SerializedName("toPay")
    @Expose
    private Double toPay;


    @Override
    public void addOnPropertyChangedCallback(
            OnPropertyChangedCallback callback) {
        registry.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(
            OnPropertyChangedCallback callback) {
        registry.remove(callback);
    }

    @Bindable
    public Double getToPay() {
        return toPay;
    }

    public void setToPay(Double toPay) {
        this.toPay = toPay;
    }

    @Bindable
    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Payment() {
        this.card = 0.0;
        this.cash = 0.0;
        this.done = false;
        this.amount = 0.0;
        this.loan = 0.0;

    }

    public Payment(Double card, Double cash, Boolean done, Double amount) {

        this.card = card;
        this.cash = cash;
        this.done = done;
        this.amount = amount;
    }
    @Bindable
    public Double getCard() {

        return card;
    }

    public void setCard(Double card) {
        this.card = card;
    }
    @Bindable
    public Double getCash() {
        return cash;
    }

    public void setCash(Double cash) {
        this.cash = cash;
    }
    @Bindable
    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    @SerializedName("done")
    @Expose
    private Boolean done;
}
